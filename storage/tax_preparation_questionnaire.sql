-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2019 at 02:40 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `np`
--

-- --------------------------------------------------------

--
-- Table structure for table `tax_preparation_questionnaire`
--

CREATE TABLE `tax_preparation_questionnaire` (
  `id` int(11) NOT NULL,
  `referral_code` varchar(200) NOT NULL,
  `referred_by` varchar(100) NOT NULL,
  `referral_site_company` varchar(100) NOT NULL,
  `are_you_a_new_client` varchar(20) NOT NULL,
  `filling_status` varchar(100) NOT NULL,
  `taxpayer_fname` varchar(100) NOT NULL,
  `taxpayer_lname` varchar(100) NOT NULL,
  `taxpayer_address` varchar(200) NOT NULL,
  `taxpayer_city` varchar(100) NOT NULL,
  `taxpayer_state` varchar(100) NOT NULL,
  `taxpayer_zip` int(11) NOT NULL,
  `tax_payer_date_of_birth` date NOT NULL,
  `social_security_number` int(11) NOT NULL,
  `taxpayer_occupation` varchar(100) NOT NULL,
  `taxpayer_email` varchar(100) NOT NULL,
  `taxpayer_phone` bigint(20) NOT NULL,
  `tax_payer_identification` varchar(100) NOT NULL,
  `driver_license_ss_card` varchar(200) NOT NULL,
  `irs_ip_pin` varchar(100) NOT NULL,
  `are_you_married` varchar(50) NOT NULL,
  `spouse_fname` varchar(100) NOT NULL,
  `spouse_lname` varchar(100) NOT NULL,
  `spouse_date_of_birth` date NOT NULL,
  `spouse_social_security_number` int(11) NOT NULL,
  `spouse_occupation` varchar(100) NOT NULL,
  `spouse_identification` varchar(100) NOT NULL,
  `spouse_driver_s_license_ss_card` varchar(200) NOT NULL,
  `do_you_have_any_children` varchar(10) NOT NULL,
  `how_many_children` varchar(50) NOT NULL,
  `dependent_1_fname` varchar(100) NOT NULL,
  `dependent_1_lname` varchar(100) NOT NULL,
  `dependent_1_date_of_birth` date NOT NULL,
  `dependent_1_social_security_number` int(11) NOT NULL,
  `dependent_1_relationship_to_you` varchar(100) NOT NULL,
  `dependent_1_social_security_card` varchar(200) NOT NULL,
  `dependent_2_fname` varchar(100) NOT NULL,
  `dependent_2_lname` varchar(100) NOT NULL,
  `dependent_2_date_of_birth` date NOT NULL,
  `dependent_2_social_security_number` int(11) NOT NULL,
  `dependent_2_relationship_to_you` varchar(100) NOT NULL,
  `dependent_2_social_security_card` varchar(200) NOT NULL,
  `dependent_3_fname` varchar(100) NOT NULL,
  `dependent_3_lname` varchar(100) NOT NULL,
  `dependent_3_date_of_birth` date NOT NULL,
  `dependent_3_social_security_number` int(11) NOT NULL,
  `dependent_3_relationship_to_you` varchar(100) NOT NULL,
  `dependent_3_social_security_card` varchar(200) NOT NULL,
  `dependent_4_fname` varchar(100) NOT NULL,
  `dependent_4_lname` varchar(100) NOT NULL,
  `dependent_4_date_of_birth` date NOT NULL,
  `dependent_4_social_security_number` int(11) NOT NULL,
  `dependent_4_relationship_to_you` varchar(100) NOT NULL,
  `dependent_4_social_security_card` varchar(200) NOT NULL,
  `dependent_care_aftercare_expenses` varchar(50) NOT NULL,
  `daycare_aftercare_for_which_dependent` varchar(100) NOT NULL,
  `amount_paid` float NOT NULL,
  `provider_name` varchar(100) NOT NULL,
  `ein` int(11) NOT NULL,
  `provider_address` varchar(200) NOT NULL,
  `provider_city` varchar(100) NOT NULL,
  `provider_state` varchar(100) NOT NULL,
  `provider_zip` int(11) NOT NULL,
  `daycare_aftercare_form` varchar(200) NOT NULL,
  `advance_up_to_6000` varchar(50) NOT NULL,
  `refund_advance_deposited` varchar(100) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `type_of_account` varchar(50) NOT NULL,
  `bank_routing_number` int(11) NOT NULL,
  `bank_account_number` int(11) NOT NULL,
  `health_insurance_coverage` varchar(50) NOT NULL,
  `dependent_have_health_insurance_coverage` varchar(50) NOT NULL,
  `healthcare_coverage_as` varchar(200) NOT NULL,
  `insurance_premium_paid` varchar(100) NOT NULL,
  `health_care_forms` varchar(200) NOT NULL,
  `attend_college` varchar(50) NOT NULL,
  `people_attending_college` varchar(50) NOT NULL,
  `person_1_name_attending_college` varchar(200) NOT NULL,
  `person_1_pay_for_college` varchar(100) NOT NULL,
  `person_1_college_name` varchar(200) NOT NULL,
  `person_1_college_form_receipts_etc` varchar(200) NOT NULL,
  `person_2_name_attending_college` varchar(200) NOT NULL,
  `person_2_pay_for_college` varchar(100) NOT NULL,
  `person_2_college_name` varchar(200) NOT NULL,
  `person_2_college_form_receipts_etc` varchar(200) NOT NULL,
  `person_3_name_attending_college` varchar(200) NOT NULL,
  `person_3_pay_for_college` varchar(100) NOT NULL,
  `person_3_college_name` varchar(200) NOT NULL,
  `person_3_college_form_receipts_etc` varchar(200) NOT NULL,
  `type_of_income` varchar(100) NOT NULL,
  `name_of_business` varchar(200) NOT NULL,
  `description_of_business` varchar(200) NOT NULL,
  `vehicle_description` varchar(200) NOT NULL,
  `date_in_service` varchar(200) NOT NULL,
  `business_miles` varchar(200) NOT NULL,
  `total_miles_driven` varchar(200) NOT NULL,
  `income_documents` varchar(200) NOT NULL,
  `self_employment_documents` varchar(200) NOT NULL,
  `employment_additional_information` text NOT NULL,
  `deductions_that_apply` varchar(200) NOT NULL,
  `deductions_doc` varchar(200) NOT NULL,
  `deductions_additional_information` text NOT NULL,
  `casualty_area` varchar(50) NOT NULL,
  `casualty_city_and_state` varchar(200) NOT NULL,
  `living_benefits_and_business_iul` varchar(50) NOT NULL,
  `interested_in_credit_restoration` varchar(50) NOT NULL,
  `interested_in_retirement_planning` varchar(50) NOT NULL,
  `due_diligence_compliance` varchar(500) NOT NULL,
  `qualifying_dependent` text NOT NULL,
  `signature` varchar(200) NOT NULL,
  `date` varchar(50) NOT NULL,
  `print_taxpayer_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tax_preparation_questionnaire`
--
ALTER TABLE `tax_preparation_questionnaire`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tax_preparation_questionnaire`
--
ALTER TABLE `tax_preparation_questionnaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
