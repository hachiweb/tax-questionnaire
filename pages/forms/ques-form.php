<?php
error_reporting(0);
$auth ="public";
include('../../header.php');
$ref = $_GET['ref'];
$view = $_GET['view'];
$D = "";
$H = "display:none;";
$FID = "";
$db = new DB();
if (!empty($view)) {
    $sql = "SELECT * FROM `tax_preparation_questionnaire` WHERE `id` = '$view'";
    $result = $db->executeQuery($sql);
    $tax = mysqli_fetch_array($result);
    $D = "disabled";
    $H = "";
    $FID = "display:none;";
    $tax0 = $tax['id'];
    $tax1   = (!empty($tax['referral_code']))? $tax['referral_code']: "";
    $tax2   = (!empty($tax['referred_by']))? $tax['referred_by']: "";
    $tax3   = (!empty($tax['referral_site_company']))? $tax['referral_site_company']: "";
    $tax4   = (!empty($tax['are_you_a_new_client']))? $tax['are_you_a_new_client']: "";
    $tax5   = (!empty($tax['filling_status']))? $tax['filling_status']: "";
    $tax6   = (!empty($tax['taxpayer_fname']))? $tax['taxpayer_fname']: "";
    $tax7   = (!empty($tax['taxpayer_lname']))? $tax['taxpayer_lname']: "";
    $tax8   = (!empty($tax['taxpayer_address']))? $tax['taxpayer_address']: "";
    $tax9   = (!empty($tax['taxpayer_city']))? $tax['taxpayer_city']: "";
    $tax10  = (!empty($tax['taxpayer_state']))? $tax['taxpayer_state']: "";
    $tax11  = (!empty($tax['taxpayer_zip']))? $tax['taxpayer_zip']: "";
    $tax12  = (!empty($tax['tax_payer_date_of_birth']))? $tax['tax_payer_date_of_birth']: "";
    $tax13  = (!empty($tax['social_security_number']))? $tax['social_security_number']: "";
    $tax14  = (!empty($tax['taxpayer_occupation']))? $tax['taxpayer_occupation']: "";
    $tax15  = (!empty($tax['taxpayer_email']))? $tax['taxpayer_email']: "";
    $tax16  = (!empty($tax['taxpayer_phone']))? $tax['taxpayer_phone']: "";
    $tax17  = (!empty($tax['tax_payer_identification']))? $tax['tax_payer_identification']: "";
    $tax18  = (!empty($tax['driver_license_ss_card']))? (explode(",",$tax['driver_license_ss_card'])): "";
    $tax19  = (!empty($tax['irs_ip_pin']))? $tax['irs_ip_pin']: "";
    $tax20  = (!empty($tax['are_you_married']))? $tax['are_you_married']: "";
    $tax21  = (!empty($tax['spouse_fname']))? $tax['spouse_fname']: "";
    $tax22  = (!empty($tax['spouse_lname']))? $tax['spouse_lname']: "";
    $tax23  = (!empty($tax['spouse_date_of_birth']))? $tax['spouse_date_of_birth']: "";
    $tax24  = (!empty($tax['spouse_social_security_number']))? $tax['spouse_social_security_number']: "";
    $tax25  = (!empty($tax['spouse_occupation']))? $tax['spouse_occupation']: "";
    $tax26  = (!empty($tax['spouse_identification']))? $tax['spouse_identification']: "";
    $tax27  = (!empty($tax['spouse_driver_s_license_ss_card']))? (explode(",",$tax['spouse_driver_s_license_ss_card'])): "";
    $tax28  = (!empty($tax['do_you_have_any_children']))? $tax['do_you_have_any_children']: "";
    $tax29  = (!empty($tax['how_many_children']))? $tax['how_many_children']: "";
    $tax30  = (!empty($tax['dependent_1_fname']))? $tax['dependent_1_fname']: "";
    $tax31  = (!empty($tax['dependent_1_lname']))? $tax['dependent_1_lname']: "";
    $tax32  = (!empty($tax['dependent_1_date_of_birth']))? $tax['dependent_1_date_of_birth']: "";
    $tax33  = (!empty($tax['dependent_1_social_security_number']))? $tax['dependent_1_social_security_number']: "";
    $tax34  = (!empty($tax['dependent_1_relationship_to_you']))? $tax['dependent_1_relationship_to_you']: "";
    $tax35  = (!empty($tax['dependent_1_social_security_card']))? (explode(",",$tax['dependent_1_social_security_card'])): "";
    $tax36  = (!empty($tax['dependent_2_fname']))? $tax['dependent_2_fname']: "";
    $tax37  = (!empty($tax['dependent_2_lname']))? $tax['dependent_2_lname']: "";
    $tax38  = (!empty($tax['dependent_2_date_of_birth']))? $tax['dependent_2_date_of_birth']: "";
    $tax39  = (!empty($tax['dependent_2_social_security_number']))? $tax['dependent_2_social_security_number']: "";
    $tax40  = (!empty($tax['dependent_2_relationship_to_you']))? $tax['dependent_2_relationship_to_you']: "";
    $tax41  = (!empty($tax['dependent_2_social_security_card']))? (explode(",",$tax['dependent_2_social_security_card'])): "";
    $tax42  = (!empty($tax['dependent_3_fname']))? $tax['dependent_3_fname']: "";
    $tax43  = (!empty($tax['dependent_3_lname']))? $tax['dependent_3_lname']: "";
    $tax44  = (!empty($tax['dependent_3_date_of_birth']))? $tax['dependent_3_date_of_birth']: "";
    $tax45  = (!empty($tax['dependent_3_social_security_number']))? $tax['dependent_3_social_security_number']: "";
    $tax46  = (!empty($tax['dependent_3_relationship_to_you']))? $tax['dependent_3_relationship_to_you']: "";
    $tax47  = (!empty($tax['dependent_3_social_security_card']))? (explode(",",$tax['dependent_3_social_security_card'])): "";
    $tax48  = (!empty($tax['dependent_4_fname']))? $tax['dependent_4_fname']: "";
    $tax49  = (!empty($tax['dependent_4_lname']))? $tax['dependent_4_lname']: "";
    $tax50  = (!empty($tax['dependent_4_date_of_birth']))? $tax['dependent_4_date_of_birth']: "";
    $tax51  = (!empty($tax['dependent_4_social_security_number']))? $tax['dependent_4_social_security_number']: "";
    $tax52  = (!empty($tax['dependent_4_relationship_to_you']))? $tax['dependent_4_relationship_to_you']: "";
    $tax53  = (!empty($tax['dependent_4_social_security_card']))? (explode(",",$tax['dependent_4_social_security_card'])): "";
    $tax54  = (!empty($tax['dependent_care_aftercare_expenses']))? $tax['dependent_care_aftercare_expenses']: "";
    $tax55  = (!empty($tax['daycare_aftercare_for_which_dependent']))? (explode(",",$tax['daycare_aftercare_for_which_dependent'])): "";
    $tax56  = (!empty($tax['amount_paid']))? $tax['amount_paid']: "";
    $tax57  = (!empty($tax['provider_name']))? $tax['provider_name']: "";
    $tax58  = (!empty($tax['ein']))? $tax['ein']: "";
    $tax59  = (!empty($tax['provider_address']))? $tax['provider_address']: "";
    $tax60  = (!empty($tax['provider_city']))? $tax['provider_city']: "";
    $tax61  = (!empty($tax['provider_state']))? $tax['provider_state']: "";
    $tax62  = (!empty($tax['provider_zip']))? $tax['provider_zip']: "";
    $tax63  = (!empty($tax['daycare_aftercare_form']))? (explode(",",$tax['daycare_aftercare_form'])): "";
    $tax64  = (!empty($tax['advance_up_to_6000']))? $tax['advance_up_to_6000']: "";
    $tax65  = (!empty($tax['refund_advance_deposited']))? $tax['refund_advance_deposited']: "";
    $tax66  = (!empty($tax['bank_name']))? $tax['bank_name']: "";
    $tax67  = (!empty($tax['type_of_account']))? $tax['type_of_account']: "";
    $tax68  = (!empty($tax['bank_routing_number']))? $tax['bank_routing_number']: "";
    $tax69  = (!empty($tax['bank_account_number']))? $tax['bank_account_number']: "";
    $tax70  = (!empty($tax['health_insurance_coverage']))? $tax['health_insurance_coverage']: "";
    $tax71  = (!empty($tax['dependent_have_health_insurance_coverage']))? $tax['dependent_have_health_insurance_coverage']: "";
    $tax72  = (!empty($tax['healthcare_coverage_as']))? $tax['healthcare_coverage_as']: "";
    $tax73  = (!empty($tax['insurance_premium_paid']))? $tax['insurance_premium_paid']: "";
    $tax74  = (!empty($tax['health_care_forms']))? (explode(",",$tax['health_care_forms'])): "";
    $tax75  = (!empty($tax['attend_college']))? $tax['attend_college']: "";
    $tax76  = (!empty($tax['people_attending_college']))? $tax['people_attending_college']: "";
    $tax77  = (!empty($tax['person_1_name_attending_college']))? $tax['person_1_name_attending_college']: "";
    $tax78  = (!empty($tax['person_1_pay_for_college']))? $tax['person_1_pay_for_college']: "";
    $tax79  = (!empty($tax['person_1_college_name']))? $tax['person_1_college_name']: "";
    $tax80  = (!empty($tax['person_1_college_form_receipts_etc']))? (explode(",",$tax['person_1_college_form_receipts_etc'])): "";
    $tax81  = (!empty($tax['person_2_name_attending_college']))? $tax['person_2_name_attending_college']: "";
    $tax82  = (!empty($tax['person_2_pay_for_college']))? $tax['person_2_pay_for_college']: "";
    $tax83  = (!empty($tax['person_2_college_name']))? $tax['person_2_college_name']: "";
    $tax84  = (!empty($tax['person_2_college_form_receipts_etc']))? (explode(",",$tax['person_2_college_form_receipts_etc'])): "";
    $tax85  = (!empty($tax['person_3_name_attending_college']))? $tax['person_3_name_attending_college']: "";
    $tax86  = (!empty($tax['person_3_pay_for_college']))? $tax['person_3_pay_for_college']: "";
    $tax87  = (!empty($tax['person_3_college_name']))? $tax['person_3_college_name']: "";
    $tax88  = (!empty($tax['person_3_college_form_receipts_etc']))? (explode(",",$tax['person_3_college_form_receipts_etc'])): "";
    $tax89  = (!empty($tax['type_of_income']))? (explode(",",$tax['type_of_income'])): "";
    $tax90  = (!empty($tax['name_of_business']))? $tax['name_of_business']: "";
    $tax91  = (!empty($tax['description_of_business']))? $tax['description_of_business']: "";
    $tax92  = (!empty($tax['vehicle_description']))? $tax['vehicle_description']: "";
    $tax93  = (!empty($tax['date_in_service']))? $tax['date_in_service']: "";
    $tax94  = (!empty($tax['business_miles']))? $tax['business_miles']: "";
    $tax95  = (!empty($tax['total_miles_driven']))? $tax['total_miles_driven']: "";
    $tax96  = (!empty($tax['income_documents']))? (explode(",",$tax['income_documents'])): "";
    $tax97  = (!empty($tax['self_employment_documents']))? (explode(",",$tax['self_employment_documents'])): "";
    $tax98  = (!empty($tax['employment_additional_information']))? $tax['employment_additional_information']: "";
    $tax99  = (!empty($tax['deductions_that_apply']))? (explode(",",$tax['deductions_that_apply'])): "";
    $tax100 = (!empty($tax['deductions_doc']))? (explode(",",$tax['deductions_doc'])): "";
    $tax101 = (!empty($tax['deductions_additional_information']))? $tax['deductions_additional_information']: "";
    $tax102 = (!empty($tax['casualty_area']))? $tax['casualty_area']: "";
    $tax103 = (!empty($tax['casualty_city_and_state']))? $tax['casualty_city_and_state']: "";
    $tax104 = (!empty($tax['living_benefits_and_business_iul']))? $tax['living_benefits_and_business_iul']: "";
    $tax105 = (!empty($tax['interested_in_credit_restoration']))? $tax['interested_in_credit_restoration']: "";
    $tax106 = (!empty($tax['interested_in_retirement_planning']))? $tax['interested_in_retirement_planning']: "";
    $tax107 = (!empty($tax['due_diligence_compliance']))? (explode(",",$tax['due_diligence_compliance'])): "";
    $tax108 = (!empty($tax['qualifying_dependent']))? $tax['qualifying_dependent']: "";
    $tax109 = (!empty($tax['signature']))? explode(",",$tax['signature']): "";
    $tax110 = (!empty($tax['date']))? explode(",",$tax['date']): "";
    $tax111 = (!empty($tax['print_taxpayer_name']))? explode(",",$tax['print_taxpayer_name']): "";
}
if ($tax20=="No") {
    $SpouseH = "display:none;";
}elseif($tax20=="Yes"){
    $SpouseH = "";
}else{
    $SpouseH = "display:none;";
}
if ($tax28=="No") {
    $DepenH = "display:none;";
}elseif($tax28=="Yes"){
    $DepenH = "";
}else{
    $DepenH = "display:none;";
}
if($tax29=="One") {
    $Child1H = "";
    $Child2H = "display:none;";
    $Child3H = "display:none;";
    $Child4H = "display:none;";
}elseif($tax29=="Two") {
    $Child1H = "";
    $Child2H = "";
    $Child3H = "display:none;";
    $Child4H = "display:none;";
}elseif($tax29=="Three") {
    $Child1H = "";
    $Child2H = "";
    $Child3H = "";
    $Child4H = "display:none;";
}elseif($tax29=="Four") {
    $Child1H = "";
    $Child2H = "";
    $Child3H = "";
    $Child4H = "";
}else{
    $Child1H = "display:none;";
    $Child2H = "display:none;";
    $Child3H = "display:none;";
    $Child4H = "display:none;";
}
if ($tax54=="No") {
    $DacH = "display:none;";
}elseif($tax54=="Yes"){
    $DacH = "";
}else{
    $DacH = "display:none;";
}
if (!empty($tax55)) {
    if (in_array("Dependent One", $tax55)){
        $Acd1H = "checked";
    } else {
        $Acd1H = "";
    }
    if (in_array("Dependent Two", $tax55)){
        $Acd2H = "checked";
    } else {
        $Acd2H = "";
    }
    if (in_array("Dependent Three", $tax55)){
        $Acd3H = "checked";
    } else {
        $Acd3H = "";
    }
    if (in_array("Dependent Four", $tax55)){
        $Acd4H = "checked";
    } else {
        $Acd4H = "";
    }
} else {
    $Acd1H = "";
    $Acd2H = "";
    $Acd3H = "";
    $Acd4H = "";
}
if ($tax65=="Checking / Savings") {
    $RadH = "";
}else{
    $RadH = "display:none;";
}
if ($tax72=="Employer") {
    $HccH = "";
}else{
    $HccH = "display:none;";
}
if($tax76=="One") {
    $Ac1H = "";
    $Ac2H = "display:none;";
    $Ac3H = "display:none;";
}elseif($tax76=="Two") {
    $Ac1H = "";
    $Ac2H = "";
    $Ac3H = "display:none;";
}elseif($tax76=="Three") {
    $Ac1H = "";
    $Ac2H = "";
    $Ac3H = "";
}else{
    $Ac1H = "display:none;";
    $Ac2H = "display:none;";
    $Ac3H = "display:none;";
}
if (!empty($tax89)) {
    if (in_array("W2", $tax89)){
        $Toi1H = "checked";
    } else {
        $Toi1H = "";
    }
    if (in_array("Self Employment", $tax89)){
        $Toi2H = "checked";
    } else {
        $Toi2H = "";
    }
    if (in_array("1099 MISC", $tax89)){
        $Toi3H = "checked";
    } else {
        $Toi3H = "";
    }
    if (in_array("Unemployment / 1099G", $tax89)){
        $Toi4H = "checked";
    } else {
        $Toi4H = "";
    }
    if (in_array("Pension", $tax89)){
        $Toi5H = "checked";
    } else {
        $Toi5H = "";
    }
    if (in_array("Alimony", $tax89)){
        $Toi6H = "checked";
    } else {
        $Toi6H = "";
    }
    if (in_array("Social Security", $tax89)){
        $Toi7H = "checked";
    } else {
        $Toi7H = "";
    }
    if (in_array("Other", $tax89)){
        $Toi8H = "checked";
    } else {
        $Toi8H = "";
    }
    if (in_array("None", $tax89)){
        $Toi9H = "checked";
    } else {
        $Toi9H = "";
    }   
} else {
    $Toi1H = "";
    $Toi2H = "";
    $Toi3H = "";
    $Toi4H = "";
    $Toi5H = "";
    $Toi6H = "";
    $Toi7H = "";
    $Toi8H = "";
    $Toi9H = "";
}
if (!empty($Toi2H)) {
    $SedH = "";
}
else {
    $SedH = "display:none;";
}
if (!empty($tax99)) {
    if (in_array("Medical Expenses", $tax99)){
        $Dta1H = "checked";
    } else {
        $Dta1H = "";
    }
    if (in_array("Property Taxes", $tax99)){
        $Dta2H = "checked";
    } else {
        $Dta2H = "";
    }
    if (in_array("School Interest", $tax99)){
        $Dta3H = "checked";
    } else {
        $Dta3H = "";
    }
    if (in_array("Investment Interest", $tax99)){
        $Dta4H = "checked";
    } else {
        $Dta4H = "";
    }
    if (in_array("Business Loan Interest", $tax99)){
        $Dta5H = "checked";
    } else {
        $Dta5H = "";
    }
    if (in_array("Interest / Mortage", $tax99)){
        $Dta6H = "checked";
    } else {
        $Dta6H = "";
    }
    if (in_array("State Refund", $tax99)){
        $Dta7H = "checked";
    } else {
        $Dta7H = "";
    }
    if (in_array("Contributions / Donations", $tax99)){
        $Dta8H = "checked";
    } else {
        $Dta8H = "";
    }
    if (in_array("Business Car Interest", $tax99)){
        $Dta9H = "checked";
    } else {
        $Dta9H = "";
    } 
    if (in_array("None", $tax99)){
        $Dta10H = "checked";
    } else {
        $Dta10H = "";
    }   
} else {
    $Dta1H = "";
    $Dta2H = "";
    $Dta3H = "";
    $Dta4H = "";
    $Dta5H = "";
    $Dta6H = "";
    $Dta7H = "";
    $Dta8H = "";
    $Dta9H = "";
    $Dta10H = "";
}
if ($tax102=="No") {
    $DaaH = "display:none;";
}elseif($tax102=="Yes"){
    $DaaH = "";
}else{
    $DaaH = "display:none;";
}
if (!empty($tax107)) {
    if (in_array("You can provide documentation to substantiate eligibility for the credit(s) and/or HOH status", $tax107)){
        $Ddc1H = "checked";
    } else {
        $Ddc1H = "";
    }
    if (in_array("No one else can claim the depenpent(s) you have listed", $tax107)){
        $Ddc2H = "checked";
    } else {
        $Ddc2H = "";
    }
    if (in_array("The dependent(s) that you are claiming lived with you for over half of the year", $tax107)){
        $Ddc3H = "checked";
    } else {
        $Ddc3H = "";
    }
    if (in_array("You are unmarried or considered unamrried on the last day of the tax year and you have provided more than half of the cost of keeping up thhome", $tax107)){
        $Ddc4H = "checked";
    } else {
        $Ddc4H = "";
    }
    if (in_array("If self employed / Misc: you can provide income and expense documents", $tax107)){
        $Ddc5H = "checked";
    } else {
        $Ddc5H = "";
    }
    if (in_array("If in school you can provide 1098T form and/or receipts for the qualified tuition and related expenses", $tax107)){
        $Ddc6H = "checked";
    } else {
        $Ddc6H = "";
    }
    if (in_array("None Applies", $tax107)){
        $Ddc7H = "checked";
    } else {
        $Ddc7H = "";
    }  
} else {
    $Ddc1H = "";
    $Ddc2H = "";
    $Ddc3H = "";
    $Ddc4H = "";
    $Ddc5H = "";
    $Ddc6H = "";
    $Ddc7H = "";
}
$ref_sql = "SELECT * FROM `referral` WHERE `referral_code` = '$ref'";
$ref_result = $db->executeQuery($ref_sql);
$ref_tax = mysqli_fetch_array($ref_result);
?>

<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Questionnaire Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
              <li class="breadcrumb-item active">Questionnaire Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="container">
        <form onsubmit="return validateForm(event)" action="ques-sub.php" method="post" id="tax-que-form" enctype="multipart/form-data" >
            <div class="row border box-shadow">
                <div class="mx-3">
                <div class="py-4">
                    <h2 class="bolder d-inline">TAX PREPARATION QUESTIONNAIRE</h2>
                    <button type="button" class="btn btn-primary btn-md txt-btn float-right">
                        <a href="ref-register.php" class="text-white text-decoration-none px-2">Get Shareable Link</a>
                    </button>
                </div>
                    <p class="">Thank you for taking the time to complete out tax prep questionnaire. When you have completed
                        all applicable parts of this form you must click the "SUBMIT" button at the bottom of the form to send
                        this information to our office. Upon clicking the "SUBMIT" button you will receive a confirmation email
                    that we received your information.</p>

                    <strong class=" text-justify text-clr">*Closing your browser before clicking
                        "Submit" button will delete all information * Please click SAVE bottom right if you have to come back to
                    the form*</strong>
                    <!-- <p for="" class='mb-0 mt-4 text-clr'>Referral Site/Company: *</p> -->
                    <div class="row mt-4">
                        <div>
                            <input type="hidden" name="referral_code" value="<?=(!empty($ref_tax['referral_code']))?$ref_tax['referral_code']:""?>">
                            <input type="hidden" name="referred_by" value="<?=(!empty($ref_tax['name']))?$ref_tax['name']:""?>">
                           <input type="hidden" name="referral_site_company" value="<?=(!empty($ref_tax['company']))?$ref_tax['company']:""?>">
                        </div>
                        <div class="col-sm-6">
                            <p class="text-clr">Filling Status *</p>
                            <!-- radio -->
                            <div class="form-group">
                                <?php
                                if (!empty($tax5)) {?>
                                   <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" checked>
                                    <label class="custom-control-label font-weight-normal"><?=$tax5;?></label>
                                </div>
                                <?php
                                } else {?>
                                    <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio1" name="filling_status" value="Single">
                                    <label for="customRadio1" class="custom-control-label font-weight-normal">Single</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio2" name="filling_status" value="Married">
                                    <label for="customRadio2" class="custom-control-label font-weight-normal">Married</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio3" name="filling_status" value="Married Filing Jointly">
                                    <label for="customRadio3" class="custom-control-label font-weight-normal">Married Filing
                                    Jointly</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio4" name="filling_status" value="Head of Household">
                                    <label for="customRadio4" class="custom-control-label font-weight-normal">Head of
                                    Household</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio5" name="filling_status" value="Widow">
                                    <label for="customRadio5" class="custom-control-label font-weight-normal">Widow</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <p class="text-clr">Are you a new client</p>
                            <div class="form-group form-inline">
                            <?php
                                if (!empty($tax4)) {?>
                                   <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" checked>
                                    <label class="custom-control-label font-weight-normal"><?=$tax4;?></label>
                                </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio6" name="are_you_a_new_client" value="Yes">
                                    <label for="customRadio6" class="custom-control-label font-weight-normal">Yes</label>
                                </div>
                                <div class="custom-control custom-radio ml-5">
                                    <input class="custom-control-input" type="radio" id="customRadio7" name="are_you_a_new_client" value="No">
                                    <label for="customRadio7" class="custom-control-label font-weight-normal pl-0" >No</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>


                        </div>
                    </div>

                    <div class="row mx-1">
                        <div class="col-sm-12">
                            <h4 class="txt-color">Taxpayer Information</h4>
                            <span class="mb-0 text-clr">Taxpayer's Name (as it appears on Social Security Card)*</span>
                            <div class="d-flex">
                                <input type="text" class="form-control corner-zero mr-2" placeholder="First" name="taxpayer_fname" value="<?=$tax6;?>" <?=$D;?>>
                                <input type="text" class="form-control corner-zero ml-2" placeholder="Last" name="taxpayer_lname" value="<?=$tax7;?>" <?=$D;?>>
                            </div>
                            <p class="mb-0 mt-3 ml-1 text-clr">Address*</p>
                            <input type="text" class="form-control corner-zero" placeholder="Address Line 1" name="taxpayer_address" value="<?=$tax8;?>" <?=$D;?>>
                            <div class="d-flex mt-3">
                                <input type="text" class="form-control corner-zero mr-1" placeholder="City" name="taxpayer_city" value="<?=$tax9;?>" <?=$D;?>>
                                <select name="taxpayer_state" id="taxpayer_state" class="form-control corner-zero" <?=$D;?>>
                                    <option value="<?=$tax10;?>" selected><?=(!empty($tax10))?$tax10:"State";?></option>
                                    <option value="Armed Forces America">Armed Forces America</option>
                                    <option value="Armed Forces">Armed Forces</option>
                                    <option value="Armed Forces Pacific">Armed Forces Pacific</option>
                                    <option value="Alabama">Alabama</option>
                                    <option value="Alaska">Alaska</option>
                                    <option value="Arizona">Arizona</option>
                                    <option value="Arkansas">Arkansas</option>
                                    <option value="California">California</option>
                                    <option value="Colorado">Colorado</option>
                                    <option value="Connecticut">Connecticut</option>
                                    <option value="District of Columbia">District of Columbia</option>
                                    <option value="Delaware">Delaware</option>
                                    <option value="Florida">Florida</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Guam">Guam</option>
                                    <option value="Hawaii">Hawaii</option>
                                    <option value="Idaho">Idaho</option>
                                    <option value="Illinois">Illinois</option>
                                    <option value="Indiana">Indiana</option>
                                    <option value="Iowa">Iowa</option>
                                    <option value="Kansas">Kansas</option>
                                    <option value="Kentucky">Kentucky</option>
                                    <option value="Louisiana">Louisiana</option>
                                    <option value="Maine">Maine</option>
                                    <option value="Maryland">Maryland</option>
                                    <option value="Massachusetts">Massachusetts</option>
                                    <option value="Michigan">Michigan</option>
                                    <option value="Minnesota">Minnesota</option>
                                    <option value="Mississippi">Mississippi</option>
                                    <option value="Missouri">Missouri</option>
                                    <option value="Montana">Montana</option>
                                    <option value="Nebraska">Nebraska</option>
                                    <option value="New Hampshire">New Hampshire</option>
                                    <option value="New Jersey">New Jersey</option>
                                    <option value="New Mexico">New Mexico</option>
                                    <option value="New York">New York</option>
                                    <option value="Nevada">Nevada</option>
                                    <option value="North Carolina">North Carolina</option>
                                    <option value="North Dakota">North Dakota</option>
                                    <option value="Ohio">Ohio</option>
                                    <option value="Oklahoma">Oklahoma</option>
                                    <option value="Oregon">Oregon</option>
                                    <option value="Pennsylvania">Pennsylvania</option>
                                    <option value="Puerto Rico">Puerto Rico</option>
                                    <option value="Rhode Island">Rhode Island</option>
                                    <option value="South Carolina">South Carolina</option>
                                    <option value="South Dakota">South Dakota</option>
                                    <option value="Tennessee">Tennessee</option>
                                    <option value="Texas">Texas</option>
                                    <option value="Utah">Utah</option>
                                    <option value="Vermont">Vermont</option>
                                    <option value="Virgin Islands">Virgin Islands</option>
                                    <option value="Virginia">Virginia</option>
                                    <option value="Washington">Washington</option>
                                    <option value="West Virginia">West Virginia</option>
                                    <option value="Wisconsin">Wisconsin</option>
                                    <option value="Wyoming">Wyoming</option>
                                </select>
                                <input type="number" class="form-control corner-zero ml-1" placeholder="Zip Code" name="taxpayer_zip" value="<?=$tax11;?>" <?=$D;?>>
                            </div>
                        </div>

                    </div>
                    <div class="row mx-1 mt-3">
                        <div class="col-sm-3">
                            <span class="text-clr">Tax Payer's Date of Birth* </span>
                            <input type="date" class="form-control corner-zero" name="tax_payer_date_of_birth" value="<?=$tax12;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-3">
                            <span class="text-clr">Social Security Number* </span>
                            <input type="number" class="form-control corner-zero" name="social_security_number" value="<?=$tax13;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-6">
                            <span class="text-clr">Taxpayer's Occupation* </span>
                            <input type="text" class="form-control corner-zero" name="taxpayer_occupation" value="<?=$tax14;?>" <?=$D;?>>
                        </div>
                    </div>
                    <div class="row mx-1 mt-3">
                        <div class="col-sm-6">
                            <span class="text-clr">Email* </span>
                            <input type="email" class="form-control corner-zero" name="taxpayer_email" value="<?=$tax15;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-6">
                            <span class="text-clr">Phone* </span>
                            <input type="number" class="form-control corner-zero" name="taxpayer_phone" value="<?=$tax16;?>" <?=$D;?>>
                        </div>
                    </div>
                    <div class="row mx-1 mt-3">
                        <div class="col-sm-3">
                            <span class="text-clr">Tax Payer's Identification*</span>
                            <!-- radio -->
                            <div class="form-group">
                            <?php
                                if (!empty($tax17)) {?>
                                   <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" checked>
                                    <label class="custom-control-label font-weight-normal"><?=$tax17;?></label>
                                </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio8" name="tax_payer_identification" value="State Driver License" >
                                    <label for="customRadio8" class="custom-control-label font-weight-normal">State Driver's
                                    License</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio9" name="tax_payer_identification" value="State ID Card" >
                                    <label for="customRadio9" class="custom-control-label font-weight-normal">State ID
                                    Card</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio10" name="tax_payer_identification" value="US passport" >
                                    <label for="customRadio10" class="custom-control-label font-weight-normal">US
                                    passport</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio11" name="tax_payer_identification" value="US Military ID" >
                                    <label for="customRadio11" class="custom-control-label font-weight-normal">US Military
                                    ID</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="customRadio12" name="tax_payer_identification" value="US Resident Alien ID" >
                                    <label for="customRadio12" class="custom-control-label font-weight-normal">US Resident Alien
                                    ID</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                        if (!empty($tax18)) {?>
                            <div class="col-sm-6">
                            <span class="text-clr">Uoloaded Documents </span>
                            <?php for ($i=0; $i <count($tax18) ; $i++) { ?>
                            <div class="">
                                <a href="<?=$site_url."/upload/".$tax0."/".$tax18[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax18[$i]; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        }else {?>
                        <div class="col-sm-6" style="<?=$FID;?>">
                            <span class="text-clr">Please upload copy of your Driver's License & SS Card* </span>
                            <div class="">
                                <input type="file" accept="image/*" id="upload_file" name="driver_license_ss_card[]" onchange="preview_image();" style="background-color:rgb(245,245,245);padding:10px;" multiple>
                                <div>
                                    <span id="image_preview"></span>
                                </div>

                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        <div class="col-sm-3">
                            <span class="text-clr">IRS IP PIN* </span>
                            <input type="text" class="form-control corner-zero" name="irs_ip_pin" value="<?=$tax19;?>" <?=$D;?>>
                            <p class="txt-italic">IRS provides this number</p>
                        </div>
                    </div>
                    <div class="row mx-1 mt-3">
                        <div class="col-sm-2">
                            <span class="text-clr">Are You Married*</span>
                            <div class="form-group">
                            <?php
                                if (!empty($tax17)) {?>
                                   <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" checked>
                                    <label class="custom-control-label font-weight-normal"><?=$tax20;?></label>
                                </div>
                                <?php
                                } else {?>
                            <div class="custom-control custom-radio">
                                <input class="custom-control-input" type="radio" id="customRadio13" name="are_you_married" value="Yes">
                                <label for="customRadio13" class="custom-control-label font-weight-normal"
                                id="yes-show">Yes</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input class="custom-control-input" type="radio" id="customRadio14" name="are_you_married" value="No">
                                <label for="customRadio14" class="custom-control-label font-weight-normal pl-0"
                                id="no-show">No</label>
                            </div>
                            <?php
                            }
                            ?>

                            </div>
                        </div>
                        <div class="col-sm-5 Spouse-show" style="<?=$SpouseH;?>">
                            <span class="mb-0 text-clr">Spouse Name (as it appears on Social Security Card)*</span>
                            <input type="text" class="form-control corner-zero" placeholder="First" name="spouse_fname" value="<?=$tax21;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-5 pt-4 Spouse-show" style="<?=$SpouseH;?>">
                            <input type="text" class="form-control corner-zero" placeholder="Last" name="spouse_lname" value="<?=$tax22;?>" <?=$D;?>>
                        </div>


                    </div>

                    <div class="row mx-1 mt-3 Spouse-show" style="<?=$SpouseH;?>">
                        <div class="col-sm-4">
                            <span class="text-clr">Spouse's Date of Birth *</span>
                            <input type="date" class="form-control corner-zero" placeholder="D.O.B" name="spouse_date_of_birth" value="<?=$tax23;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-4">
                            <span class="mb-0 text-clr">Spouse's Social Security Number*</span>
                            <input type="number" class="form-control corner-zero" placeholder="Social Security Number" name="spouse_social_security_number" value="<?=$tax24;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-4">
                            <span class="mb-0 text-clr">Spouse Occupation*</span>
                            <input type="text" class="form-control corner-zero" placeholder="Occupation" name="spouse_occupation" value="<?=$tax25;?>" <?=$D;?>>
                        </div>

                    </div>

                    <div class="row mx-1 mt-3 Spouse-show" style="<?=$SpouseH;?>">
                        <div class="col-sm-6">
                            <span class="text-clr">Spouse Idenitification*</span>
                            <!-- radio -->
                            <div class="form-group">
                            <?php
                                if (!empty($tax26)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax26;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="custom1" name="spouse_identification" value="State Drivers License">
                                    <label for="custom1" class="custom-control-label font-weight-normal">State Driver's
                                    License</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="custom2" name="spouse_identification" value="State ID Card">
                                    <label for="custom2" class="custom-control-label font-weight-normal">State ID Card</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="custom3" name="spouse_identification" value="US passport">
                                    <label for="custom3" class="custom-control-label font-weight-normal">US passport</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="custom4" name="spouse_identification" value="US Military ID">
                                    <label for="custom4" class="custom-control-label font-weight-normal">US Military ID</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="custom5" name="spouse_identification" value="US Resident Alien ID">
                                    <label for="custom5" class="custom-control-label font-weight-normal">US Resident Alien
                                    ID</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                        if (!empty($tax27)) {?>
                            <div class="col-sm-6">
                            <span class="text-clr">Uoloaded Documents </span>
                            <?php for ($i=0; $i <count($tax27) ; $i++) { ?>
                            <div class="">
                                <a href="<?=$site_url."/upload/".$tax0."/".$tax27[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax27[$i]; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        }else {?>
                        <div class="col-sm-6" style="<?=$FID;?>">
                            <span class="text-clr">Please upload copy of your Driver's License & SS Card* </span>
                            <div class="">
                                <input type="file" accept="image/*" name="image[]" class="custom-file-input" id="customFile" multiple>
                                <input type="file" accept="image/*" id="upload_file1" name="spouse_driver_s_license_ss_card[]" onchange="preview_image1();" style="background-color:rgb(245,245,245);padding:10px;" multiple>
                                <div>
                                    <span id="image_preview1"></span>
                                </div>

                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        
                    </div>

                    <div class="row mx-1 mt-3">
                        <div class="col-sm-12">
                            <h4 class="txt-color">Dependent Information</h4>
                        </div>
                        <div class="col-sm-4">
                            <span class="text-clr">Do you have any children or dependents?*</span>
                            <!-- radio -->
                            <div class="form-group">
                            <?php
                                if (!empty($tax28)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax28;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="children1" name="do_you_have_any_children" value="Yes">
                                    <label for="children1" class="custom-control-label font-weight-normal" id="yes-show-1">Yes</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="children2" name="do_you_have_any_children" value="No">
                                    <label for="children2" class="custom-control-label font-weight-normal" id="no-show-1">No</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-8" style="<?=$DepenH;?>" id="dependents">
                            <span class="text-clr">How many children or dependents do you have?*</span>
                            <!-- radio -->
                            <div class="form-group">
                            <?php
                                if (!empty($tax29)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax29;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="dependents1" name="how_many_children" value="One">
                                    <label for="dependents1" class="custom-control-label font-weight-normal" id="one">One</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="dependents2" name="how_many_children" value="Two">
                                    <label for="dependents2" class="custom-control-label font-weight-normal" id="two">Two</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="dependents3" name="how_many_children" value="Three">
                                    <label for="dependents3" class="custom-control-label font-weight-normal" id="three">Three</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="dependents4" name="how_many_children" value="Four">
                                    <label for="dependents4" class="custom-control-label font-weight-normal" id="four">Four</label>
                                </div>
                            </div>
                                <?php
                                }
                                ?>                            
                        </div>
                    </div>
                    <div class="row mx-1 mt-3" style="<?=$Child1H;?>" id="dependent-1">
                        <div class="col-sm-12">
                            <h4 class="txt-color">Dependent One</h4>
                        </div>
                        <div class="col-sm-6">
                            <span class="text-clr">Dependent #1 Name (as it appears on social security card)</span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_1_fname" placeholder="First" value="<?=$tax30;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-6 pt-4">
                            <span class=""></span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_1_lname" placeholder="Last" value="<?=$tax31;?>" <?=$D;?>>
                            </div>
                        </div>

                        <div class="col-sm-3 mt-3">
                            <span class="text-clr">Dependent #1 Date of Birth</span>
                            <div class="form-group">
                                <input type="date" class="form-control corner-zero" name="dependent_1_date_of_birth" placeholder="First" value="<?=$tax32;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-4 mt-3">
                            <span class="text-clr">Dependent #1 Social Security Number</span>
                            <div class="form-group">
                                <input type="number" class="form-control corner-zero" name="dependent_1_social_security_number" placeholder="Security Number" value="<?=$tax33;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-5 mt-3">
                            <span class="text-clr">Dependent #1 Relationship to you (son, daughter, etc)</span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_1_relationship_to_you" value="<?=$tax34;?>" <?=$D;?>>
                            </div>
                        </div>
                        <?php
                        if (!empty($tax35)) {?>
                            <div class="col-sm-5 mt-3">
                                <span class="text-clr">Uoloaded Documents </span>
                                <?php for ($i=0; $i <count($tax35) ; $i++) { ?>
                                <div class="">
                                    <a href="<?=$site_url."/upload/".$tax0."/".$tax35[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax35[$i]; ?></a>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        <?php
                        }else {?>
                           <div class="col-sm-5 mt-3" style="<?=$FID;?>">
                            <span class="text-clr">Please upload Dependent #1 Social Security Card</span>
                            <div class="form-group">
                                <div class="">
                                    <input type="file" accept="image/*" id="upload_file2" name="dependent_1_social_security_card[]" onchange="preview_image2();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                                    <div>
                                        <span id="image_preview2"></span>
                                    </div>

                                </div>
                            </div>
                        </div> 
                        <?php
                        }
                        ?>
                    </div>

                    <div class="row mx-1 mt-3" style="<?=$Child2H;?>" id="dependent-2">
                        <div class="col-sm-12">
                            <h4 class="txt-color">Dependent Two</h4>
                        </div>
                        <div class="col-sm-6">
                            <span class="text-clr">Dependent #2 Name (as it appears on social security card)</span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_2_fname" placeholder="First" value="<?=$tax36;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-6 pt-4">
                            <span class=""></span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_2_lname" placeholder="Last" value="<?=$tax37;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-3 mt-3">
                            <span class="text-clr">Dependent #2 Date of Birth</span>
                            <div class="form-group">
                                <input type="date" class="form-control corner-zero" name="dependent_2_date_of_birth" value="<?=$tax38;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-4 mt-3">
                            <span class="text-clr">Dependent #2 Social Security Number</span>
                            <div class="form-group">
                                <input type="number" class="form-control corner-zero" name="dependent_2_social_security_number" placeholder="Security Number" value="<?=$tax39;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-5 mt-3">
                            <span class="text-clr">Dependent #2 Relationship to you (son, daughter, etc)</span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_2_relationship_to_you" value="<?=$tax40;?>" <?=$D;?> >
                            </div>
                        </div>
                        <?php
                        if (!empty($tax41)) {?>
                            <div class="col-sm-5 mt-3">
                                <span class="text-clr">Uoloaded Documents </span>
                                <?php for ($i=0; $i <count($tax41) ; $i++) { ?>
                                <div class="">
                                    <a href="<?=$site_url."/upload/".$tax0."/".$tax41[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax41[$i]; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        }else {?>
                        <div class="col-sm-5 mt-3" style="<?=$FID;?>">
                            <span class="text-clr">Please upload Dependent #2 Social Security Card</span>
                            <div class="form-group">
                                <div class="">
                                    <input type="file" accept="image/*" id="upload_file3" name="dependent_2_social_security_card[]" onchange="preview_image3();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                                    <div>
                                        <span id="image_preview3"></span>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                    </div>

                    <div class="row mx-1 mt-3"  style="<?=$Child3H;?>" id="dependent-3">
                        <div class="col-sm-12">
                            <h4 class="txt-color">Dependent Three</h4>
                        </div>
                        <div class="col-sm-6">
                            <span class="text-clr">Dependent #3 Name (as it appears on social security card)</span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_3_fname" placeholder="First" value="<?=$tax42;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-6 pt-4">
                            <span class=""></span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_3_lname" placeholder="Last" value="<?=$tax43;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-3 mt-3">
                            <span class="text-clr">Dependent #3 Date of Birth</span>
                            <div class="form-group">
                                <input type="date" class="form-control corner-zero" name="dependent_3_date_of_birth" value="<?=$tax44;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-4 mt-3">
                            <span class="text-clr">Dependent #3 Social Security Number</span>
                            <div class="form-group">
                                <input type="number" class="form-control corner-zero" name="dependent_3_social_security_number" placeholder="Security Number" value="<?=$tax45;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-5 mt-3">
                            <span class="text-clr">Dependent #3 Relationship to you (son, daughter, etc)</span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_3_relationship_to_you" value="<?=$tax46;?>" <?=$D;?>>
                            </div>
                        </div>
                        <?php
                        if (!empty($tax47)) {?>
                            <div class="col-sm-5 mt-3">
                                <span class="text-clr">Uoloaded Documents </span>
                                <?php for ($i=0; $i <count($tax47) ; $i++) { ?>
                                <div class="">
                                    <a href="<?=$site_url."/upload/".$tax0."/".$tax47[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax47[$i]; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        }else {?>
                           <div class="col-sm-5 mt-3" style="<?=$FID;?>">
                            <span class="text-clr">Please upload Dependent #3 Social Security Card</span>
                            <div class="form-group">
                                <div class="">
                                    <input type="file" accept="image/*" id="upload_file4" name="dependent_3_social_security_card[]" onchange="preview_image4();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                                    <div>
                                        <span id="image_preview4"></span>
                                    </div>

                                </div>
                            </div>
                        </div> 
                        <?php
                        }
                        ?>
                    </div>

                    <div class="row mx-1 mt-3" style="<?=$Child4H;?>" id="dependent-4">
                        <div class="col-sm-12">
                            <h4 class="txt-color">Dependent Four</h4>
                        </div>
                        <div class="col-sm-6">
                            <span class="text-clr">Dependent #4 Name (as it appears on social security card)</span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_4_fname" placeholder="First" value="<?=$tax48;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-6 pt-4">
                            <span class=""></span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_4_lname" placeholder="Last" value="<?=$tax49;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-3 mt-3">
                            <span class="text-clr">Dependent #4 Date of Birth</span>
                            <div class="form-group">
                                <input type="date" class="form-control corner-zero" name="dependent_4_date_of_birth" value="<?=$tax50;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-4 mt-3">
                            <span class="text-clr">Dependent #4 Social Security Number</span>
                            <div class="form-group">
                                <input type="number" class="form-control corner-zero" name="dependent_4_social_security_number" placeholder="Security Number" value="<?=$tax51;?>" <?=$D;?>>
                            </div>
                        </div>
                        <div class="col-sm-5 mt-3">
                            <span class="text-clr">Dependent #4 Relationship to you (son, daughter, etc)</span>
                            <div class="form-group">
                                <input type="text" class="form-control corner-zero" name="dependent_4_relationship_to_you" value="<?=$tax52;?>" <?=$D;?>>
                            </div>
                        </div>
                        <?php
                        if (!empty($tax53)) {?>
                            <div class="col-sm-5 mt-3">
                                <span class="text-clr">Uoloaded Documents </span>
                                <?php for ($i=0; $i <count($tax53) ; $i++) { ?>
                                <div class="">
                                    <a href="<?=$site_url."/upload/".$tax0."/".$tax53[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax53[$i]; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        }else {?>
                            <div class="col-sm-5 mt-3" style="<?=$FID;?>">
                                <span class="text-clr">Please upload Dependent #4 Social Security Card</span>
                                <div class="form-group">
                                    <div class="">
                                        <input type="file" accept="image/*" id="upload_file5" name="dependent_4_social_security_card[]" onchange="preview_image5();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                                        <div>
                                            <span id="image_preview5"></span>
                                        </div>
    
                                    </div>
                                </div>
                            </div>    
                        <?php
                        }
                        ?>
                    </div>

                    <div class="row mx-1 mt-3">
                        <div class="col-sm-12">
                            <h4 class="txt-color">Dependent Care Expenses</h4>
                        </div>
                        <div class="col-sm-5">
                            <span class="text-clr">Do you have Dependent Care / Aftercare Expenses*</span>
                            <!-- radio -->
                            <div class="form-group">
                            <?php
                                if (!empty($tax54)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax54;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Care1" name="dependent_care_aftercare_expenses" value="Yes">
                                    <label for="Care1" class="custom-control-label font-weight-normal" id="yes-show-2">Yes</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Care2" name="dependent_care_aftercare_expenses" value="No">
                                    <label for="Care2" class="custom-control-label font-weight-normal" id="no-show-2">No</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-7 daycare" style="<?=$DacH;?>">
                            <span class="text-clr">Daycare / Aftercare for which dependent*</span>
                            <!-- radio -->
                            <div class="form-group">
                            <?php 
                            if (!empty($Acd1H)) { ?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="Aftercare1" name="daycare_aftercare_for_which_dependent[]" value="Dependent One" <?=$Acd1H?>>
                                    <label for="Aftercare1" class="custom-control-label font-weight-normal">Dependent
                                    One</label>
                                </div>
                            <?php
                            } if (!empty($Acd2H)) {?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="Aftercare2" name="daycare_aftercare_for_which_dependent[]" value="Dependent Two" <?=$Acd2H?>>
                                    <label for="Aftercare2" class="custom-control-label font-weight-normal">Dependent
                                    Two</label>
                                </div>
                            <?php
                            } if (!empty($Acd3H)) {?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="Aftercare3" name="daycare_aftercare_for_which_dependent[]" value="Dependent Three" <?=$Acd3H?>>
                                    <label for="Aftercare3" class="custom-control-label font-weight-normal">Dependent
                                    Three</label>
                                </div>
                            <?php
                            } if (!empty($Acd4H)) {?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="Aftercare4" name="daycare_aftercare_for_which_dependent[]" value="Dependent Four" <?=$Acd4H?>>
                                    <label for="Aftercare4" class="custom-control-label font-weight-normal">Dependent
                                    Four</label>
                                </div>
                            <?php
                            } elseif(empty($tax55)){?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="Aftercare1" name="daycare_aftercare_for_which_dependent[]" value="Dependent One">
                                    <label for="Aftercare1" class="custom-control-label font-weight-normal">Dependent
                                    One</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="Aftercare2" name="daycare_aftercare_for_which_dependent[]" value="Dependent Two">
                                    <label for="Aftercare2" class="custom-control-label font-weight-normal">Dependent
                                    Two</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="Aftercare3" name="daycare_aftercare_for_which_dependent[]" value="Dependent Three">
                                    <label for="Aftercare3" class="custom-control-label font-weight-normal">Dependent
                                    Three</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="Aftercare4" name="daycare_aftercare_for_which_dependent[]" value="Dependent Four">
                                    <label for="Aftercare4" class="custom-control-label font-weight-normal">Dependent
                                    Four</label>
                                </div>
                            <?php
                            }
                            ?>
                            </div>
                        </div>
                    </div>

                    <div class="row mx-1 mt-3 daycare" style="<?=$DacH;?>">
                        <div class="col-sm-6">

                            <div class="form-group">
                                <span class="text-clr">Amount Paid required*</span>
                                <input type="number" class="form-control corner-zero" name="amount_paid" value="<?=$tax56;?>" <?=$D;?>>
                            </div>
                        </div>

                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            <span class="text-clr">Provider's Name *</span>
                            <input type="text" class="form-control corner-zero" name="provider_name" placeholder="Name" value="<?=$tax57;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-6">
                            <span class="text-clr">EIN (Employer Identification Number) *</span>
                            <input type="number" class="form-control corner-zero" name="ein" placeholder="Number" value="<?=$tax58;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-12 mt-3">
                            <span class="text-clr">Provider's Address*</span>
                            <input type="text" class="form-control corner-zero" name="provider_address" placeholder="Address Line 1" value="<?=$tax59;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-4 mt-3">
                            <span class="text-clr"></span>
                            <input type="text" class="form-control corner-zero" name="provider_city" placeholder="City" value="<?=$tax60;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-4 mt-3">
                            <select name="provider_state" id="provider_state" class="form-control corner-zero"<?=$D;?>>
                            <option value="<?=$tax61;?>" selected><?=(!empty($tax61))?$tax61:"State";?></option>
                                <option value="Armed Forces America">Armed Forces America</option>
                                <option value="Armed Forces">Armed Forces</option>
                                <option value="Armed Forces Pacific">Armed Forces Pacific</option>
                                <option value="Alabama">Alabama</option>
                                <option value="Alaska">Alaska</option>
                                <option value="Arizona">Arizona</option>
                                <option value="Arkansas">Arkansas</option>
                                <option value="California">California</option>
                                <option value="Colorado">Colorado</option>
                                <option value="Connecticut">Connecticut</option>
                                <option value="District of Columbia">District of Columbia</option>
                                <option value="Delaware">Delaware</option>
                                <option value="Florida">Florida</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Guam">Guam</option>
                                <option value="Hawaii">Hawaii</option>
                                <option value="Idaho">Idaho</option>
                                <option value="Illinois">Illinois</option>
                                <option value="Indiana">Indiana</option>
                                <option value="Iowa">Iowa</option>
                                <option value="Kansas">Kansas</option>
                                <option value="Kentucky">Kentucky</option>
                                <option value="Louisiana">Louisiana</option>
                                <option value="Maine">Maine</option>
                                <option value="Maryland">Maryland</option>
                                <option value="Massachusetts">Massachusetts</option>
                                <option value="Michigan">Michigan</option>
                                <option value="Minnesota">Minnesota</option>
                                <option value="Mississippi">Mississippi</option>
                                <option value="Missouri">Missouri</option>
                                <option value="Montana">Montana</option>
                                <option value="Nebraska">Nebraska</option>
                                <option value="New Hampshire">New Hampshire</option>
                                <option value="New Jersey">New Jersey</option>
                                <option value="New Mexico">New Mexico</option>
                                <option value="New York">New York</option>
                                <option value="Nevada">Nevada</option>
                                <option value="North Carolina">North Carolina</option>
                                <option value="North Dakota">North Dakota</option>
                                <option value="Ohio">Ohio</option>
                                <option value="Oklahoma">Oklahoma</option>
                                <option value="Oregon">Oregon</option>
                                <option value="Pennsylvania">Pennsylvania</option>
                                <option value="Puerto Rico">Puerto Rico</option>
                                <option value="Rhode Island">Rhode Island</option>
                                <option value="South Carolina">South Carolina</option>
                                <option value="South Dakota">South Dakota</option>
                                <option value="Tennessee">Tennessee</option>
                                <option value="Texas">Texas</option>
                                <option value="Utah">Utah</option>
                                <option value="Vermont">Vermont</option>
                                <option value="Virgin Islands">Virgin Islands</option>
                                <option value="Virginia">Virginia</option>
                                <option value="Washington">Washington</option>
                                <option value="West Virginia">West Virginia</option>
                                <option value="Wisconsin">Wisconsin</option>
                                <option value="Wyoming">Wyoming</option>
                            </select>
                        </div>
                        <div class="col-sm-4 mt-3">
                            <span class="text-clr"></span>
                            <input type="text" class="form-control corner-zero" name="provider_zip" placeholder="Zip Code" value="<?=$tax62;?>" <?=$D;?>>
                        </div>
                        <?php
                        if (!empty($tax63)) {?>
                            <div class="col-sm-6 mt-3">
                                <span class="text-clr">Uoloaded Documents </span>
                                <?php for ($i=0; $i <count($tax63) ; $i++) { ?>
                                <div class="">
                                    <a href="<?=$site_url."/upload/".$tax0."/".$tax63[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax63[$i]; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        }else {?>
                            <div class="col-sm-6 mt-3" style="<?=$FID;?>">
                                <span class="text-clr">Please upload Daycare / Aftercare form</span>
                                <div class="">
                                    <input type="file" accept="image/*" id="upload_file6" name="daycare_aftercare_form[]" onchange="preview_image6();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                                    <div>
                                        <span id="image_preview6"></span>
                                    </div>
    
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>

                    <div class="row mx-1 mt-3">
                        <div class="col-sm-12">
                            <h4 class="txt-color">Refund and Advance Deposit Information</h4>
                        </div>
                        <div class="col-sm-5">
                            <span class="text-clr">Would you like to apply for the advance up to $6000?*</span>
                            <!-- radio -->
                            <div class="form-group">
                            <?php
                                if (!empty($tax64)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax64;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Refund1" name="advance_up_to_6000" value="Yes">
                                    <label for="Refund1" class="custom-control-label font-weight-normal">Yes</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Refund2" name="advance_up_to_6000" value="No">
                                    <label for="Refund2" class="custom-control-label font-weight-normal">No</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <span class="text-clr">How would you like your Refund / Advance deposited?*</span>
                            <!-- radio -->
                            <div class="form-group">
                            <?php
                                if (!empty($tax65)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax65;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="deposited1" name="refund_advance_deposited" value="Checking / Savings">
                                    <label for="deposited1" class="custom-control-label font-weight-normal Checking">Checking /
                                    Savings</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="deposited2" name="refund_advance_deposited" value="Walmart Direct">
                                    <label for="deposited2" class="custom-control-label font-weight-normal Checking-1">Walmart
                                    Direct</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="deposited3" name="refund_advance_deposited" value="Printed Check">
                                    <label for="deposited3" class="custom-control-label font-weight-normal Checking-1">Printed
                                    Check</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="deposited4" name="refund_advance_deposited" value="Netspend Card">
                                    <label for="deposited4" class="custom-control-label font-weight-normal Checking-1">Netspend
                                    Card</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row mx-1 mt-3" style="<?=$RadH;?>" id="bank">
                        <div class="col-sm-6">
                            <span class="text-clr">Bank Name*</span>
                            <input type="text" class="form-control corner-zero" name="bank_name" value="<?=$tax66;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-5">
                            <span class="text-clr">Type of Account*</span>
                            <!-- radio -->
                            <div class="form-group form-inline">
                            <?php
                                if (!empty($tax67)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax67;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Checking1" name="type_of_account" value="Checking">
                                    <label for="Checking1" class="custom-control-label font-weight-normal">Checking</label>
                                </div>
                                <div class="custom-control custom-radio ml-4">
                                    <input class="custom-control-input" type="radio" id="Checking2" name="type_of_account" value="Savings">
                                    <label for="Checking2" class="custom-control-label font-weight-normal">Savings</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>

                        </div>
                        <div class="col-sm-6 mt-3">
                            <span class="text-clr">Bank Routing Number *</span>
                            <input type="number" class="form-control corner-zero" name="bank_routing_number" value="<?=$tax68;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-6 mt-3">
                            <span class="text-clr">Bank Account Number *</span>
                            <input type="number" class="form-control corner-zero" name="bank_account_number" value="<?=$tax69;?>" <?=$D;?>>
                        </div>
                    </div>

                    <div class="row mx-1 mt-3">
                        <div class="col-sm-12">
                            <h4 class="txt-color">ACA/Health Insurance Compliance Questions</h4>
                        </div>
                        <div class="col-sm-12">
                            <span class="text-clr">Did you have health insurance coverage for all 12 months of the tax year for
                            which we are preparing tax returns?</span>
                            <div class="form-group">
                            <?php
                                if (!empty($tax70)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax70;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="insurance1" name="health_insurance_coverage" value="Yes">
                                    <label for="insurance1" class="custom-control-label font-weight-normal">Yes</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="insurance2" name="health_insurance_coverage" value="No">
                                    <label for="insurance2" class="custom-control-label font-weight-normal">No</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <span class="text-clr">Did your Dependent(s) have health insurance coverage for all 12 months of the
                            tax year for which we are preparing tax returns?*</span>
                            <div class="form-group form-inline">
                            <?php
                                if (!empty($tax71)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax71;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="coverage1" name="dependent_have_health_insurance_coverage" value="Yes">
                                    <label for="coverage1" class="custom-control-label font-weight-normal">Yes</label>
                                </div>
                                <div class="custom-control custom-radio ml-4">
                                    <input class="custom-control-input" type="radio" id="coverage2" name="dependent_have_health_insurance_coverage" value="No">
                                    <label for="coverage2" class="custom-control-label font-weight-normal">No</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <span class="text-clr">Select any of the following which describes how you (or any other family
                            member on this return) received healthcare coverage</span>
                            <div class="form-group">
                            <?php
                                if (!empty($tax72)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax72;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="describes1" name="healthcare_coverage_as" value="Employer">
                                    <label for="describes1" class="custom-control-label font-weight-normal" id="employer">Employer</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="describes2" name="healthcare_coverage_as" value="Medicaid, Medicare, Veterans Benefits">
                                    <label for="describes2" class="custom-control-label font-weight-normal Medicare">Medicaid, Medicare,
                                    Veterans Benefits</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="describes3" name="healthcare_coverage_as" value='Private Insurance (Not through the "Marketplace")'>
                                    <label for="describes3" class="custom-control-label font-weight-normal Medicare">Private Insurance (Not through the "Marketplace")</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="describes4" name="healthcare_coverage_as" value="Marketplace (Obama Care)">
                                    <label for="describes4" class="custom-control-label font-weight-normal Medicare">Marketplace (Obama
                                    Care)</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3 mt-3" style="<?=$HccH;?>" id="Insurance">
                            <span class="text-clr">Insurance Premium Paid</span>
                            <input type="text" class="form-control corner-zero" name="insurance_premium_paid" value="<?=$tax73;?>" <?=$D;?>>
                        </div>
                        <?php
                        if (!empty($tax74)) {?>
                            <div class="col-sm-6 mt-3">
                                <span class="text-clr">Uoloaded Documents </span>
                                <?php for ($i=0; $i <count($tax74) ; $i++) { ?>
                                <div class="">
                                    <a href="<?=$site_url."/upload/".$tax0."/".$tax74[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax74[$i]; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        }else {?>
                            <div class="col-sm-6 mt-3" style="<?=$FID;?>">
                                <span class="text-clr">Health Care Forms (1095 A, 1095 B, 1095 C)</span>
                                <div class="">
                                    <input type="file" accept="image/*" id="upload_file7" name="health_care_forms[]" onchange="preview_image7();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                                    <div>
                                        <span id="image_preview7"></span>
                                    </div>
    
                                </div>
                            </div>
                            
                        <?php
                        }
                        ?>
                        <div class="col-sm-6"></div>

                    </div>

                    <div class="row mx-1 mt-3">
                        <div class="col-sm-12">
                            <h4 class="txt-color">Education Information</h4>
                        </div>
                        <div class="col-sm-6">
                            <span class="text-clr">Did you or your dependent attend College ?</span>
                            <!-- radio -->
                            <div class="form-group">
                            <?php
                                if (!empty($tax75)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax75;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="College1" name="attend_college" value="Yes">
                                    <label for="College1" class="custom-control-label font-weight-normal" id="yesCollege">Yes</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="College2" name="attend_college" value="No">
                                    <label for="College2" class="custom-control-label font-weight-normal" id="noCollege">No</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6" style="<?=$H;?>" id="household">
                            <span class="text-clr">How many people in your household is attending college *</span>
                            <!-- radio -->
                            <div class="form-group form-inline">
                            <?php
                                if (!empty($tax76)) {?>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" checked>
                                        <label class="custom-control-label font-weight-normal"><?=$tax76;?></label>
                                    </div>
                                <?php
                                } else {?>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="household1" name="people_attending_college" value="One">
                                    <label for="household1" class="custom-control-label font-weight-normal" id="people1">One</label>
                                </div>
                                <div class="custom-control custom-radio ml-4">
                                    <input class="custom-control-input" type="radio" id="household2" name="people_attending_college" value="Two">
                                    <label for="household2" class="custom-control-label font-weight-normal" id="people2">Two</label>
                                </div>
                                <div class="custom-control custom-radio ml-4">
                                    <input class="custom-control-input" type="radio" id="household3" name="people_attending_college" value="Three">
                                    <label for="household3" class="custom-control-label font-weight-normal" id="people3">Three</label>
                                </div>
                                <?php
                                }
                                ?>
                            </div>

                        </div>
                    </div>

                    <div class="row mx-1 mt-3" style="<?=$Ac1H;?>" id="attending1">
                        <div class="col-sm-6">
                            <span class="text-clr">Name of Person #1 attending College *</span>
                            <input type="text" class="form-control corner-zero" name="person_1_name_attending_college" value="<?=$tax77;?>" <?=$D;?>>
                        </div>
                        <div class="col-sm-6">
                            <span class="text-clr">How did you pay for College? *</span>
                            <select name="person_1_pay_for_college" id="person_1_pay_for_college" class="form-control corner-zero" <?=$D;?>>
                                 <option value="<?=$tax76;?>" selected><?=(!empty($tax76))?$tax76:"Select";?></option>
                                <option value="Financial Aid">Financial Aid</option>
                                <option value="Student Loans">Student Loans</option>
                                <option value="Out of Pocket">Out of Pocket</option>
                                <option value="Scholarships">Scholarships</option>
                                <option value="Grants">Grants</option>
                                <option value="I do not know">I don't know</option>
                            </select>
                        </select>
                    </div>
                    <div class="col-sm-6 mt-3">
                        <span class="text-clr">What was the name of the College? *</span>
                        <input type="text" class="form-control corner-zero" name="person_1_college_name" value="<?=$tax79;?>" <?=$D;?>>
                    </div>
                    <?php
                        if (!empty($tax80)) {?>
                            <div class="col-sm-6 mt-3">
                                <span class="text-clr">Uoloaded Documents </span>
                                <?php for ($i=0; $i <count($tax80) ; $i++) { ?>
                                <div class="">
                                    <a href="<?=$site_url."/upload/".$tax0."/".$tax80[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax80[$i]; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        }else {?>
                            <div class="col-sm-6 mt-3" style="<?=$FID;?>">
                        <span class="text-clr">College Form 1098T, Receipts, ETC *</span>
                        <div class="">
                            <input type="file" accept="image/*" id="upload_file8" name="person_1_college_form_receipts_etc[]" onchange="preview_image8();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                            <div>
                                <span id="image_preview8"></span>
                            </div>

                        </div>
                    </div>
                        <?php
                        }
                        ?>
                    
                </div>

                <div class="row mx-1 mt-3" style="<?=$Ac2H;?>" id="attending2">
                    <div class="col-sm-6">
                        <span class="text-clr">Name of Person #2 attending College *</span>
                        <input type="text" class="form-control corner-zero" name="person_2_name_attending_college" value="<?=$tax81;?>" <?=$D;?>>
                    </div>
                    <div class="col-sm-6">
                        <span class="text-clr">How did you pay for College? *</span>
                        <select name="person_2_pay_for_college" id="person_2_pay_for_college" class="form-control corner-zero" <?=$D;?>>
                             <option value="<?=$tax82;?>" selected><?=(!empty($tax82))?$tax82:"Select";?></option>
                            <option value="Financial Aid">Financial Aid</option>
                            <option value="Student Loans">Student Loans</option>
                            <option value="Out of Pocket">Out of Pocket</option>
                            <option value="Scholarships">Scholarships</option>
                            <option value="Grants">Grants</option>
                            <option value="I do not know">I don't know</option>
                        </select>
                    </select>
                </div>
                <div class="col-sm-6 mt-3">
                    <span class="text-clr">What was the name of the College? *</span>
                    <input type="text" class="form-control corner-zero" name="person_2_college_name" value="<?=$tax83;?>" <?=$D;?>>
                </div>
                <?php
                if (!empty($tax84)) {?>
                    <div class="col-sm-6 mt-3">
                        <span class="text-clr">Uoloaded Documents </span>
                        <?php for ($i=0; $i <count($tax84) ; $i++) { ?>
                        <div class="">
                            <a href="<?=$site_url."/upload/".$tax0."/".$tax84[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax84[$i]; ?></a>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <?php
                }else {?>
                    <div class="col-sm-6 mt-3" style="<?=$FID;?>">
                        <span class="text-clr">College Form 1098T, Receipts, ETC *</span>
                        <div class="">
                            <input type="file" accept="image/*" id="upload_file9" name="person_2_college_form_receipts_etc[]" onchange="preview_image9();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                            <div>
                                <span id="image_preview9"></span>
                            </div>
    
                        </div>
                    </div>   
                <?php
                }
                ?>
            </div>

            <div class="row mx-1 mt-3" style="<?=$Ac3H;?>" id="attending3">
                <div class="col-sm-6">
                    <span class="text-clr">Name of Person #3 attending College *</span>
                    <input type="text" class="form-control corner-zero" name="person_3_name_attending_college" value="<?=$tax85;?>" <?=$D;?>>
                </div>
                <div class="col-sm-6">
                    <span class="text-clr">How did you pay for College? *</span>
                    <select name="person_3_pay_for_college" id="person_3_pay_for_college" class="form-control corner-zero" <?=$D;?>>
                         <option value="<?=$tax86;?>" selected><?=(!empty($tax86))?$tax86:"Select";?></option>
                        <option value="Financial Aid">Financial Aid</option>
                        <option value="Student Loans">Student Loans</option>
                        <option value="Out of Pocket">Out of Pocket</option>
                        <option value="Scholarships">Scholarships</option>
                        <option value="Grants">Grants</option>
                        <option value="I do not know">I don't know</option>
                    </select>
                </select>
            </div>
            <div class="col-sm-6 mt-3">
                <span class="text-clr">What was the name of the College? *</span>
                <input type="text" class="form-control corner-zero" name="person_3_college_name" value="<?=$tax87;?>" <?=$D;?>>
            </div>
            <?php
            if (!empty($tax88)) {?>
                 <div class="col-sm-6 mt-3">
                    <span class="text-clr">Uoloaded Documents </span>
                    <?php for ($i=0; $i <count($tax88) ; $i++) { ?>
                    <div class="">
                        <a href="<?=$site_url."/upload/".$tax0."/".$tax88[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax88[$i]; ?></a>
                </div>
                <?php
                }
                ?>
            </div>
            <?php
            }else {?>
                <div class="col-sm-6 mt-3" style="<?=$FID;?>">
                    <span class="text-clr">College Form 1098T, Receipts, ETC *</span>
                    <div class="">
                        <input type="file" accept="image/*" id="upload_file10" name="person_3_college_form_receipts_etc[]" onchange="preview_image10();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                        <div>
                            <span id="image_preview10"></span>
                        </div>
    
                    </div>
                </div>
            <?php
            }
            ?>
        </div>

        <div class="row mx-1 mt-3">
            <div class="col-sm-12">
                <h4 class="txt-color">Employment Information</h4>
            </div>
            <div class="col-sm-12">
                <span class="text-clr">What type of Income do you have? (SELECT ALL THAT APPLY)</span>
                <!-- radio -->
                <div class="form-group">
                <?php
                if (!empty($Toi1H)) {?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income1" name="type_of_income[]" value="W2"<?=$Toi1H;?>>
                        <label for="Income1" class="custom-control-label font-weight-normal" id="W2">W2</label>
                    </div>
                <?php
                }
                if (!empty($Toi2H)) {?>
                  <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income2" name="type_of_income[]" value="Self Employment"<?=$Toi2H;?>>
                        <label for="Income2" class="custom-control-label font-weight-normal" id="Employment">Self Employment</label>
                    </div>  
                <?php
                }
                if (!empty($Toi3H)) {?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income3" name="type_of_income[]" value="1099 MISC"<?=$Toi3H;?>>
                        <label for="Income3" class="custom-control-label font-weight-normal" id="MISC">1099 MISC</label>
                    </div>
                <?php
                }
                if (!empty($Toi4H)) {?>
                   <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income4" name="Income" value="Unemployment / 1099G"<?=$Toi4H;?>>
                        <label for="Income4" class="custom-control-label font-weight-normal">Unemployment /
                        1099G</label>
                    </div> 
                <?php
                }
                if (!empty($Toi5H)) {?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income5" name="type_of_income[]" value="Pension"<?=$Toi5H;?>>
                        <label for="Income5" class="custom-control-label font-weight-normal">Pension</label>
                    </div>
                <?php
                }
                if (!empty($Toi6H)) {?>
                   <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income6" name="type_of_income[]" value="Alimony"<?=$Toi6H;?>>
                        <label for="Income6" class="custom-control-label font-weight-normal">Alimony</label>
                    </div> 
                <?php
                }
                if (!empty($Toi7H)) {?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income7" name="type_of_income[]" value="Social Security"<?=$Toi7H;?>>
                        <label for="Income7" class="custom-control-label font-weight-normal">Social Security</label>
                    </div>
                <?php
                }
                if (!empty($Toi8H)) {?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income8" name="type_of_income[]" value="Other"<?=$Toi8H;?>>
                        <label for="Income8" class="custom-control-label font-weight-normal">Other</label>
                    </div>
                <?php
                }
                if (!empty($Toi9H)) {?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income9" name="type_of_income[]" value="None"<?=$Toi9H;?>>
                        <label for="Income9" class="custom-control-label font-weight-normal">None</label>
                    </div>
                <?php
                }if (empty($tax89)) {?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income1" name="type_of_income[]" value="W2">
                        <label for="Income1" class="custom-control-label font-weight-normal" id="W2">W2</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income2" name="type_of_income[]" value="Self Employment">
                        <label for="Income2" class="custom-control-label font-weight-normal" id="Employment">Self Employment</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income3" name="type_of_income[]" value="1099 MISC">
                        <label for="Income3" class="custom-control-label font-weight-normal" id="MISC">1099 MISC</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income4" name="Income" value="Unemployment / 1099G">
                        <label for="Income4" class="custom-control-label font-weight-normal">Unemployment /
                        1099G</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income5" name="type_of_income[]" value="Pension">
                        <label for="Income5" class="custom-control-label font-weight-normal">Pension</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income6" name="type_of_income[]" value="Alimony">
                        <label for="Income6" class="custom-control-label font-weight-normal">Alimony</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income7" name="type_of_income[]" value="Social Security">
                        <label for="Income7" class="custom-control-label font-weight-normal">Social Security</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income8" name="type_of_income[]" value="Other">
                        <label for="Income8" class="custom-control-label font-weight-normal">Other</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Income9" name="type_of_income[]" value="None">
                        <label for="Income9" class="custom-control-label font-weight-normal">None</label>
                    </div>
                <?php
                }
                ?>
                </div>
            </div>
            <div class="row" style="<?=$SedH;?>" id="show-business">
                <div class="col-sm-6 mt-3">
                    <span class="text-clr">Name of Business</span>
                    <input type="text" class="form-control corner-zero" name="name_of_business" value="<?=$tax90;?>" <?=$D;?>>
                </div>
                <div class="col-sm-6 mt-3">
                    <span class="text-clr">Description of Business</span>
                    <input type="text" class="form-control corner-zero" name="description_of_business" value="<?=$tax91;?>" <?=$D;?>>
                </div>
                <div class="col-sm-6 mt-3">
                    <span class="text-clr">Vehicle Description (Year / Make / Model)</span>
                    <input type="text" class="form-control corner-zero" name="vehicle_description" value="<?=$tax92;?>" <?=$D;?>>
                </div>
                <div class="col-sm-6 mt-3">
                    <span class="text-clr">Date in Service</span>
                    <input type="text" class="form-control corner-zero" name="date_in_service" value="<?=$tax93;?>" <?=$D;?>>
                </div>

                <div class="col-sm-6 mt-3">
                    <span class="text-clr">Business Miles</span>
                    <input type="text" class="form-control corner-zero" name="business_miles" value="<?=$tax94;?>" <?=$D;?>>
                </div>
                <div class="col-sm-6 mt-3">
                    <span class="text-clr">Total Miles Driven</span>
                    <input type="text" class="form-control corner-zero" name="total_miles_driven" value="<?=$tax95;?>" <?=$D;?>>
                </div>
                <?php
                if (!empty($tax96)) {?>
                    <div class="col-sm-6 mt-3">
                        <span class="text-clr">Uoloaded Documents </span>
                        <?php for ($i=0; $i <count($tax96) ; $i++) { ?>
                        <div class="">
                            <a href="<?=$site_url."/upload/".$tax0."/".$tax96[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax96[$i]; ?></a>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <?php
                }else {?>
                    <div class="col-sm-6 mt-3" style="<?=$FID;?>">
                        <p class="text-clr mb-0">Self Employment / 1099 MISC Documents (Income & Expenses)</p>
                        <div class="">
                            <input type="file" accept="image/*" id="upload_file11" name="self_employment_documents[]" onchange="preview_image11();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                            <div>
                                <span id="image_preview11"></span>
                            </div>
    
                        </div>
                    </div>
                <?php
                }
                ?>
                <div class="col-sm-6 mt-3">
                </div>
            </div>
            <div class="row" style="<?=$H;?>" id="show-file">
            <?php
                if (!empty($tax97)) {?>
                    <div class="col-sm-6 mt-3">
                        <span class="text-clr">Uoloaded Documents </span>
                        <?php for ($i=0; $i <count($tax97) ; $i++) { ?>
                        <div class="">
                            <a href="<?=$site_url."/upload/".$tax0."/".$tax97[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax97[$i]; ?></a>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <?php
                }else {?>
                    <div class="col-sm-6 mt-3" style="<?=$FID;?>">
                        <p class="text-clr mb-0">Income Upload</p>
                        <div class="">
                            <input type="file" accept="image/*" id="upload_file13" name="income_documents[]" onchange="preview_image13();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                            <div>
                                <span id="image_preview13"></span>
                            </div>
    
                        </div>
                    </div>
                <?php
                }
                ?>
                <div class="col-sm-6 mt-3"></div>
            </div>

            <div class="col-sm-12 mt-3">
                <p class="text-clr mb-0">Any Additional information you would like to provide?</p>
                <textarea name="employment_additional_information" id="" cols="" rows="5" class="form-control corner-zero"<?=$D;?>><?=$tax98;?></textarea>
                <span class="txt-italic">Put N/A if nothing</span>
            </div>
        </div>

        <div class="row mx-1 mt-3">
            <div class="col-sm-12">
                <h4 class="txt-color">Deductions</h4>
            </div>
            <div class="col-sm-12">
                <span class="text-clr">Please Select all deductions that apply*</span>
                <div class="row">
                    <div class="col-sm-3">
                        <!-- CHECKBOX -->
                        <div class="form-group">
                        <?php 
                            if (!empty($Dta1H)) { ?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions1"
                                    name="deductions_that_apply[]" value="Medical Expenses" <?=$Dta1H?>>
                                    <label for="deductions1" class="custom-control-label font-weight-normal" id="click-show1">Medical Expenses</label>
                                </div>
                            <?php
                            }
                            ?>
                            <?php 
                            if (!empty($Dta2H)) { ?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions2"
                                    name="deductions_that_apply[]" value="Property Taxes" <?=$Dta2H?>>
                                    <label for="deductions2" class="custom-control-label font-weight-normal" id="click-show2">Property Taxes</label>
                                </div>
                            <?php
                            }
                            ?>
                            <?php 
                            if (!empty($Dta3H)) { ?>
                              <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions3"
                                    name="deductions_that_apply[]" value="School Interest" <?=$Dta3H?>>
                                    <label for="deductions3" class="custom-control-label font-weight-normal" id="click-show3">School Interest
                                    </label>
                                </div>  
                            <?php
                            }
                            ?>
                            <?php 
                            if (!empty($Dta4H)) { ?>
                               <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions4"
                                    name="deductions_that_apply[]" value="Investment Interest" <?=$Dta4H?>>
                                    <label for="deductions4"
                                    class="custom-control-label font-weight-normal" id="click-show4">Investment Interest</label>
                                </div> 
                            <?php
                            }
                            ?>
                            <?php 
                            if (!empty($Dta5H)) { ?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions5"
                                    name="deductions_that_apply[]" value="Business Loan Interest" <?=$Dta5H?>>
                                    <label for="deductions5"
                                    class="custom-control-label font-weight-normal" id="click-show5">Business Loan Interest</label>
                                </div>
                            <?php
                            }else{
                            ?>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions1"
                                name="deductions_that_apply[]" value="Medical Expenses">
                                <label for="deductions1" class="custom-control-label font-weight-normal" id="click-show1">Medical Expenses</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions2"
                                name="deductions_that_apply[]" value="Property Taxes">
                                <label for="deductions2" class="custom-control-label font-weight-normal" id="click-show2">Property Taxes</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions3"
                                name="deductions_that_apply[]" value="School Interest">
                                <label for="deductions3" class="custom-control-label font-weight-normal" id="click-show3">School Interest
                                </label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions4"
                                name="deductions_that_apply[]" value="Investment Interest">
                                <label for="deductions4"
                                class="custom-control-label font-weight-normal" id="click-show4">Investment Interest</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions5"
                                name="deductions_that_apply[]" value="Business Loan Interest">
                                <label for="deductions5"
                                class="custom-control-label font-weight-normal" id="click-show5">Business Loan Interest</label>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <!-- CHECKBOX -->
                        <div class="form-group">
                        <?php 
                            if (!empty($Dta6H)) { ?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions6"
                                    name="deductions_that_apply[]" value="Interest / Mortage" <?=$Dta6H?>>
                                    <label for="deductions6" class="custom-control-label font-weight-normal" id="click-show6">Interest / Mortage</label>
                                </div>
                            <?php
                            }
                            ?>
                            <?php 
                            if (!empty($Dta7H)) { ?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions7"
                                    name="deductions_that_apply[]" value="State Refund" <?=$Dta7H?>>
                                    <label for="deductions7" class="custom-control-label font-weight-normal" id="click-show7">State Refund</label>
                                </div>
                            <?php
                            }
                            ?>
                            <?php 
                            if (!empty($Dta8H)) { ?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions8"
                                    name="deductions_that_apply[]" value="Contributions / Donations" <?=$Dta8H?>>
                                    <label for="deductions8" class="custom-control-label font-weight-normal" id="click-show8">Contributions / Donations</label>
                                </div>
                            <?php
                            }
                            ?>
                            <?php 
                            if (!empty($Dta9H)) { ?>
                               <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions9"
                                    name="deductions_that_apply[]" value="Business Car Interest" <?=$Dta9H?>>
                                    <label for="deductions9"
                                    class="custom-control-label font-weight-normal" id="click-show9">Business Car Interest</label>
                                </div> 
                            <?php
                            }
                            ?>
                            <?php 
                            if (!empty($Dta10H)) { ?>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="deductions10" name="deductions_that_apply[]" value="None" <?=$Dta10H?>>
                                    <label for="deductions10"
                                    class="custom-control-label font-weight-normal" id="click-show10">None</label>
                                </div>
                            <?php
                            } if (empty($tax99)) {
                            ?>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions6"
                                name="deductions_that_apply[]" value="Interest / Mortage">
                                <label for="deductions6" class="custom-control-label font-weight-normal" id="click-show6">Interest / Mortage</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions7"
                                name="deductions_that_apply[]" value="State Refund">
                                <label for="deductions7" class="custom-control-label font-weight-normal" id="click-show7">State Refund</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions8"
                                name="deductions_that_apply[]" value="Contributions / Donations">
                                <label for="deductions8" class="custom-control-label font-weight-normal" id="click-show8">Contributions / Donations</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions9"
                                name="deductions_that_apply[]" value="Business Car Interest">
                                <label for="deductions9"
                                class="custom-control-label font-weight-normal" id="click-show9">Business Car Interest</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="deductions10" name="deductions_that_apply[]" value="None">
                                <label for="deductions10"
                                class="custom-control-label font-weight-normal" id="click-show10">None</label>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                    if (!empty($tax100)) {?>
                        <div class="col-sm-6" style="<?=$FID;?>">
                            <span class="text-clr">Uoloaded Documents </span>
                            <?php for ($i=0; $i <count($tax100) ; $i++) { ?>
                            <div class="">
                                <a href="<?=$site_url."/upload/".$tax0."/".$tax100[$i]; ?>"><?=$site_url."/upload/".$tax0."/".$tax100[$i]; ?></a>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                    <?php
                    }else {?>
                        <div class="col-sm-6" style="display:none;" id="documents-show">
                            <p class="text-clr mb-0">Upload Documents</p>
                            <div class="">
                                <input type="file" accept="image/*" id="upload_file12" name="deductions_doc[]" onchange="preview_image12();" style="background-color:rgb(245,245,245);padding:10px;" multiple/>
                                <div>
                                    <span id="image_preview12"></span>
                                </div>
    
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                    <div class="col-sm-12 mt-3">
                        <p class="text-clr mb-0">Any Additional information you would like to provide?</p>
                        <textarea name="deductions_additional_information" id="" cols="" rows="5" class="form-control corner-zero" <?=$D;?>><?=$tax101;?></textarea>
                        <span class="txt-italic">Put N/A if nothing</span>
                    </div>
                </div>
            </div>
        </div>


        <div class="row mx-1 mt-3">
            <div class="col-sm-12">
                <h4 class="txt-color">Casualty & Loss</h4>
            </div>
            <div class="col-sm-6">
                <span class="text-clr">Where you located in a Diaster Affected Area?</span>
                <div class="form-group form-inline">
                <?php
                    if (!empty($tax102)) {?>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" checked>
                            <label class="custom-control-label font-weight-normal"><?=$tax102;?></label>
                        </div>
                    <?php
                    } else {?>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="Affected1" name="casualty_area" value="Yes">
                        <label for="Affected1" class="custom-control-label font-weight-normal" id="casualty-show">Yes</label>
                    </div>
                    <div class="custom-control custom-radio ml-4">
                        <input class="custom-control-input" type="radio" id="Affected2" name="casualty_area" value="No">
                        <label for="Affected2" class="custom-control-label font-weight-normal" id="casualty-no">No</label>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-sm-6" style="<?=$DaaH;?>" id="casualty">
                <span class="text-clr">City and State*</span>
                <div class="form-group">
                    <input type="text" class="form-control corner-zero" name="casualty_city_and_state" value="<?=$tax103;?>" <?=$D;?>>
                </div>
            </div>
        </div>
        <div class="row mx-1 mt-3">
            <div class="col-sm-12">
                <h4 class="txt-color">Survey Questions</h4>
            </div>
            <div class="col-sm-6">
                <span class="text-clr">Can we contact you in regards to Living Benefits and Business IUL's *</span>
                <div class="form-group form-inline">
                <?php
                    if (!empty($tax104)) {?>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" checked>
                            <label class="custom-control-label font-weight-normal"><?=$tax104;?></label>
                        </div>
                    <?php
                    } else {?>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="Benefits1" name="living_benefits_and_business_iul" value="Yes">
                        <label for="Benefits1" class="custom-control-label font-weight-normal">Yes</label>
                    </div>
                    <div class="custom-control custom-radio ml-4">
                        <input class="custom-control-input" type="radio" id="Benefits2" name="living_benefits_and_business_iul" value="No">
                        <label for="Benefits2" class="custom-control-label font-weight-normal">No</label>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <span class="text-clr">Are you interested in Credit Restoration? required*</span>
                <div class="form-group form-inline">
                <?php
                    if (!empty($tax105)) {?>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" checked>
                            <label class="custom-control-label font-weight-normal"><?=$tax105;?></label>
                        </div>
                    <?php
                    } else {?>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="Restoration1" name="interested_in_credit_restoration" value="Yes">
                        <label for="Restoration1" class="custom-control-label font-weight-normal">Yes</label>
                    </div>
                    <div class="custom-control custom-radio ml-4">
                        <input class="custom-control-input" type="radio" id="Restoration2" name="interested_in_credit_restoration" value="No">
                        <label for="Restoration2" class="custom-control-label font-weight-normal">No</label>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <span class="text-clr">Are you interested in Retirement Planning?*</span>
                <div class="form-group form-inline">
                <?php
                    if (!empty($tax106)) {?>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" checked>
                            <label class="custom-control-label font-weight-normal"><?=$tax106;?></label>
                        </div>
                    <?php
                    } else {?>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="Retirement1" name="interested_in_retirement_planning" value="Yes">
                        <label for="Retirement1" class="custom-control-label font-weight-normal">Yes</label>
                    </div>
                    <div class="custom-control custom-radio ml-4">
                        <input class="custom-control-input" type="radio" id="Retirement2" name="interested_in_retirement_planning" value="No">
                        <label for="Retirement2" class="custom-control-label font-weight-normal">No</label>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="row mx-1 mt-3">
            <div class="col-sm-12">
                <h4 class="txt-color">Due Diligence Compliance</h4>
            </div>
            <div class="col-sm-12">
                <span class="text-clr">Please select all that apply *</span>
                <!-- CHECKBOX -->
                <div class="form-group">
                <?php 
                if (!empty($Ddc1H)) { ?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence1" name="due_diligence_compliance[]" value="You can provide
                        documentation to substantiate eligibility for the credit(s) and/or HOH status"<?=$Ddc1H?>>
                        <label for="Diligence1" class="custom-control-label font-weight-normal">You can provide
                        documentation to substantiate eligibility for the credit(s) and/or HOH status</label>
                    </div>
                <?php
                }if (!empty($Ddc2H)) { ?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence2" name="due_diligence_compliance[]" value="No one else can claim the depenpent(s) you have listed"<?=$Ddc2H?>>
                        <label for="Diligence2" class="custom-control-label font-weight-normal">No one else can
                        claim the depenpent(s) you have listed</label>
                    </div>
                <?php
                }if (!empty($Ddc3H)) { ?>
                   <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence3" name="due_diligence_compliance[]" value="The dependent(s) that you are claiming lived with you for over half of the year"<?=$Ddc3H?>>
                        <label for="Diligence3" class="custom-control-label font-weight-normal">The dependent(s)
                        that you are claiming lived with you for over half of the year</label>
                    </div> 
                <?php
                }if (!empty($Ddc4H)) { ?>
                  <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence4" name="due_diligence_compliance[]" value="You are unmarried or considered unamrried on the last day of the tax year and you have provided more than half of the cost of keeping up thhome"<?=$Ddc4H?>>
                        <label for="Diligence4" class="custom-control-label font-weight-normal">You are unmarried or considered unamrried on the last day of the tax year and you have provided more than half of the cost of keeping up thhome</label>
                    </div>  
                <?php
                }if (!empty($Ddc5H)) { ?>
                   <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence5" name="due_diligence_compliance[]" value="If self employed /  Misc: you can provide income and expense documents"<?=$Ddc5H?>>
                        <label for="Diligence5" class="custom-control-label font-weight-normal">If self employed /  Misc: you can provide income and expense documents</label>
                    </div> 
                <?php
                }if (!empty($Ddc6H)) { ?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence6" name="due_diligence_compliance[]" value="If in school you can provide 1098T form and/or receipts for the qualified tuition and related expenses"<?=$Ddc6H?>>
                        <label for="Diligence6" class="custom-control-label font-weight-normal">If in school you can provide 1098T form and/or receipts for the qualified tuition and related expenses</label>
                    </div>
                <?php
                }if (!empty($Ddc7H)) { ?>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence7" name="due_diligence_compliance[]" value="None Applies"<?=$Ddc7H?>>
                        <label for="Diligence7" class="custom-control-label font-weight-normal">None Applies</label>
                    </div>
                <?php
                }if (empty($tax107)) { ?>
                   <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence1" name="due_diligence_compliance[]" value="You can provide
                        documentation to substantiate eligibility for the credit(s) and/or HOH status">
                        <label for="Diligence1" class="custom-control-label font-weight-normal">You can provide
                        documentation to substantiate eligibility for the credit(s) and/or HOH status</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence2" name="due_diligence_compliance[]" value="No one else can claim the depenpent(s) you have listed">
                        <label for="Diligence2" class="custom-control-label font-weight-normal">No one else can
                        claim the depenpent(s) you have listed</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence3" name="due_diligence_compliance[]" value="The dependent(s) that you are claiming lived with you for over half of the year">
                        <label for="Diligence3" class="custom-control-label font-weight-normal">The dependent(s)
                        that you are claiming lived with you for over half of the year</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence4" name="due_diligence_compliance[]" value="You are unmarried or considered unamrried on the last day of the tax year and you have provided more than half of the cost of keeping up thhome">
                        <label for="Diligence4" class="custom-control-label font-weight-normal">You are unmarried or considered unamrried on the last day of the tax year and you have provided more than half of the cost of keeping up thhome</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence5" name="due_diligence_compliance[]" value="If self employed /  Misc: you can provide income and expense documents">
                        <label for="Diligence5" class="custom-control-label font-weight-normal">If self employed /  Misc: you can provide income and expense documents</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence6" name="due_diligence_compliance[]" value="If in school you can provide 1098T form and/or receipts for the qualified tuition and related expenses">
                        <label for="Diligence6" class="custom-control-label font-weight-normal">If in school you can provide 1098T form and/or receipts for the qualified tuition and related expenses</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="Diligence7" name="due_diligence_compliance[]" value="None Applies">
                        <label for="Diligence7" class="custom-control-label font-weight-normal">None Applies</label>
                    </div> 
                <?php
                }
                ?>
                </div>
            </div>
            <div class="col-sm-12 mt-3">
                <p class="text-clr mb-0">If the Qualifying dependent(s) is NOT your SON or DAUGHTER - Please explain
                why the parents are not claiming them.</p>
                <textarea name="qualifying_dependent" id="" cols="" rows="5" class="form-control corner-zero"<?=$D;?>><?=$tax108;?></textarea>
            </div>
            <div class="col-sm-12">
                <h4 class="txt-color text-center">SIGN IN BOX BELOW WITH YOUR MOUSE OR A STYLUS IF USING A
                TOUCHSCREEN ENABLED DEVICE</h4>
                <div class="row">
                    <div class="col-sm-8">
                        <span class="text-clr">Signature*</span>
                        <div class="wrapper-sign">
                          <?php
                            if(isset($tax109[0]) && !empty($tax109[0])){
                            ?>
                            <img src="<?=$site_url."/upload/".$tax0."/".$tax109[0];?>" class="view-sign"/>
                            <?php 
                            } 
                           else{
                           ?> 
                            <canvas id="signature-pad" class="signature-pad" width=400 height=200></canvas>
                           <?php } ?>
                        </div>
                      <div class="sign-btn-bx">
                          <button id="clear-1" class="clear">Clear</button>
                      </div>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-clr mb-0">Date*</p>
                        <input type="text" id="datepicker" width="276" class="corner-zero" name="date[]" value="<?=$tax110[0];?>" <?=$D;?>>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">
                <p class="text-clr mb-0">Print Taxpayer's Name*</p>
                <input type="text" class="form-control corner-zero" name="print_taxpayer_name[]" value="<?=$tax111[0];?>" <?=$D;?>>
            </div>
            <div class="col-sm-12 mt-3 text-justify">
                <h4 class="txt-color text-center">Engagement Letter & Consent form</h4>
                <p>Dear Client,</p>
                <p>Thank you for the opportunity to work with you in preparing your income tax returns. To foster a
                complete understanding of our relationship, take a moment to review the following information.</p>
                <p> We will prepare your income tax returns based solely on the information you furnish to us. Upon
                    completion of your tax returns, we will return any original tax documents to you. From time to
                    time our office may retain scanned copies of your documents for our records. However you should
                    retain all documents, canceled checks and other data that form the basis of income and
                    deductions and other tax return forms, schedules, elections, and disclosures. Such documents
                    include but are not limited to, proper records to support deductions claimed for meals,
                    entertainment, travel, business gifts, charitable contributions, and vehicle use (if
                    applicable), as well as bank and credit card statements. These records will be necessary to
                    prove the accuracy and completeness of the returns to taxing authorities, should your returns be
                    selected for examination. We recommend keeping all documents and copies of your returns for a
                    minimum of five years after you file your tax returns or after their due date, whichever is
                later.</p>


                <p>Our work in connection with the preparation of your income tax returns cannot be relied upon to
                    disclose errors, irregularities or illegal acts, including, without limitation, fraud that may
                    exist within the documents or figures you provide. We will use our professional judgment in
                    resolving questions where the tax law is unclear or where there may be conflicts between the tax
                    authority’s interpretations of the tax law and other supportable positions. Unless instructed by
                    you, we will take a tax position in your favor whenever reasonable. We cannot provide any
                    assurance that tax positions taken will not be challenged or ensure the ultimate outcome of such
                    a challenge. Moreover, we cannot be responsible for issues arising from any income, expenses or
                    other information not provided to us at the time of tax preparation or prior to the filing of
                your returns.</p>

                <p> Please note that this engagement, and this firm’s services, include but are not limited to
                    general and annual income tax related services. Unless specifically requested by you and agreed
                    upon in writing we do not provide any services related to the payroll tax, sales tax, excise
                    tax, and personal or real property tax. Nor can we be relied upon to determine or report your
                    compliance, or lack thereof, with any Federal, State, or Local business-related laws, health
                    care or human resources regulations, retirement plan compliance or any business, property, or
                professional permitting/licensing.</p>

                <p> The charges for our services are on a “per-form” basis with references to time spent by our
                    professional staff to perform the work, and costs incurred for related supplies and expenses,
                    including copy charges, long distance phone charges, and computer processing charges. Our fee
                    for the preparation of your tax returns will be due and payable upon presentation of your
                    completed income tax filing. It is company policy not to release tax returns or any tax-related
                    reports, schedules, information, advice, or notes without payment in full. Should a situation
                    arise where services have been rendered but payment is not received, and our firm is forced to
                    or elects to seek legal assistance to collect fees due to us, please note that you may be asked
                    to reimburse our firm for the legal costs to collect any outstanding balance due. Also, please
                    note that an additional fee of one hundred and fifty dollars may be applied to certain accounts
                    who begin the preparation process after September 25th each year. This fee is to cover the
                    additional labor and administrative costs that arise when beginning a tax return so close to a
                    terminal filing deadline. Should arrangements in writing have been made prior to September 25th
                    or should arrangements have been made in writing with CER Financial Services at any time this
                fee may be waived upon request?</p>

                <p>Because we understand that cost is an important issue for many clients, we will do our best to
                    provide you with an estimate of our fees prior to completing your returns, but please understand
                    that circumstances arise which may raise or lower the estimated fee. We will contact you if such
                a situation arises after you receive an estimate.</p>

                <p>Upon completion of your returns and after you have paid your tax preparation fees, we will
                    provide you with a copy of your returns as well as the e-file signature authorizations and any
                    applicable payment vouchers to complete your tax filing. You should review the completed returns
                    carefully. If you see anything that requires changing, please bring it to our attention
                    immediately and we will either explain the matter to you or correct it without delay and provide
                    you with new, corrected copies. If you are satisfied with the returns and see no issues, please
                    sign and date the e-file signature authorizations and return them to this office immediately.
                    Please note that all taxing authorities prohibit us from transmitting your tax returns without
                    signed authorization from you. Please also note that if your returns cannot be e-filed, we will
                provide you with paper copies to sign and mail to the taxing authorities.</p>

                <p> By signing this engagement, you agree to compensate CER Financial Services for services you have
                    requested. In the event you terminate the engagement prior to the completion of your tax
                    accounting work, CER Financial Services reserves the right to invoice you for any out of pocket
                    expenses, time at our hourly rate, or other expenses that were incurred during this engagement.
                    If you sought in-person consultation services, received advisory services by phone or email,
                    sought representation work or received any tax accounting services, by signing this engagement
                    letter you agree to compensate CER Financial Services for these services, even if you terminated
                the engagement prior to our firm completing the work requested or required.</p>

                <p>Please be advised that if you receive any correspondence from a taxing authority that pertains
                    to a tax return prepared by this firm, we will be happy to address this matter on your behalf.
                    Included in the cost of your tax preparation is 30 minutes of correspondence work per tax year
                    at no additional charge. During that 30 minutes, we will assess your situation and apprise you
                    of the course of action we believe is best to take. If we can also compose and send off a reply
                    to the appropriate taxing authority, then we will certainly do so. If the matter or issue
                    involves more than 30 minutes of work, each additional hour (or fraction thereof) will be billed
                    at a standard hourly rate of one hundred and fifty dollars ($150) per 60-minute hour. When
                    possible, we will do our best to inform you if we believe your issue will require more than 30
                    minutes to handle. However, there may be a situation when we are unable to inform you in
                    advance. Even if we are unable to inform you, the standard hourly rate listed above will apply
                    and is payable upon completion of this work. At any time in the process, you may choose to have
                    us cease our work in connection with your correspondence, but we will require a said request in
                    writing with your name and signature included. If during the course of this work, it becomes
                    apparent the correspondence received was due to an error or omission by this office there will
                    be no charge for handling the correspondence regardless of the time involved. Please note that
                    except in cases where our office is deemed at fault, a flat $50-dollar handling charge will be
                    assessed for all matters where our assistance is requested in handling any such correspondence
                    from any taxing authority. This charge is not included in your annual tax preparation fees and
                is payable upon completion of our work in connection with said correspondence.</p>

                <p>Please also note that audit/examination representation work is not included in the tax
                    preparation fee and is considered a different engagement with a separate fee structure. If you
                    have any questions regarding the specifics of audit/examination representation, please feel free
                    to ask any time before, during or after your appointment. Again, if your returns are audited or
                    examined by any taxing authority, for any reason, the fee you paid for their preparation does
                not include the costs to represent you with regard to any audit or examination.</p>

                <p>CER Financial Services takes your privacy and personal information very seriously. We will take
                    whatever steps are necessary to safeguard that information and will never sell or disclose said
                    information to anyone outside the firm for any reason. Please note that while we will protect
                    your confidential information, our firm, from time to time, will need to utilize some of your
                    information for internal purposes not related directly to your current year tax preparation. An
                    example of this usage would be emailing you a newsletter or holiday card to your confidential
                    home address or perhaps a personal phone call to wish you or a member of your family greetings
                    or a happy birthday. By signing this letter, you agree to allow our staff limited access to your
                information for such informal, non-tax preparation related situations.</p>

                <p>Please also note that in an effort to help combat the increasing threat of identity theft and to
                    ensure the returns filed by our firm are accurate, true, and pertain to the proper client or
                    taxpayer, CER Financial Services reserves the right to request, a condition of our providing
                    services, certain documents to verify your identity and the identity of others you may be listed
                    on your tax returns. This is for your protection and for the protection of your spouse and/or
                    dependents, and these documents will be safeguarded to the best of our ability and in accordance
                    with our document protection procedures. Such documents requested by our firm may include but
                    are not limited to; government-issued photo identification, Social Security Cards, birth
                certificates, or visa and immigration-related documents.</p>

                <p>If you agree with the terms of our engagement as described in this letter, please sign below.
                    Please be aware that by signing below and giving us your income tax information, you expressly
                    agree to the terms of this engagement letter. We want to thank you for putting your trust in CER
                Financial Services and look forward to a long and mutually satisfying relationship.</p>
            </div>
            <div class="col-sm-12">
                <h4 class="txt-color text-center">SIGN IN BOX BELOW WITH YOUR MOUSE OR A STYLUS IF USING A
                TOUCHSCREEN ENABLED DEVICE</h4>
                <div class="row">
                    <div class="col-sm-8">
                        <span class="text-clr">Signature*</span>
                        <div class="wrapper-sign">
                        <?php
                            if(isset($tax109[1]) && !empty($tax109[1])){
                            ?>
                            <img src="<?=$site_url."/upload/".$tax0."/".$tax109[1];?>" class="view-sign"/>
                            <?php 
                            } 
                           else{
                           ?> 
                            <canvas id="signature-pad-1" class="signature-pad" width=400 height=200></canvas>
                           <?php } ?>
                      </div>
                      <div class="sign-btn-bx">
                          <button id="clear-2" class="clear">Clear</button>
                      </div>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-clr mb-0">Date*</p>
                        <input type="text" id="datepicker" width="276" class="corner-zero" name="date[]" value="<?=$tax110[1];?>" <?=$D;?>>
                    </div>
                </div>

            </div>
            <div class="col-sm-6">
                <p class="text-clr mb-0">Print Taxpayer's Name*</p>
                <input type="text" class="form-control corner-zero" name="print_taxpayer_name[]" value="<?=$tax111[1];?>" <?=$D;?>>
            </div>
            <div class="col-sm-12 text-justify">
                <h4 class="txt-color text-center mb-3 mt-5">CONSENT TO USE OF TAX RETURN INFORMATION</h4>
                <p>Federal law requires this consent form be provided to you.  Unless authorized by law, we cannot use, without your consent, your tax return information for purposes other than the preparation and filing of your tax return.  However, federal law also prohibits us from revealing any personal or financial information to any third party.  Therefore as a matter of fact, we will never reveal your financial or personal information in any way, to anyone, unless you specifically request we do so, and provide in writing to us the specific person or persons to whom you wish that disclose to be made, which is done on a wholly separate consent form from this.   For the purposes of this consent form please note we are seeking consent only to satisfy requirements set forth by federal law so we may speak with you on the phone, send you email correspondences of any kind, send you mailings, newsletters, email reminders, holiday and occasional cards and greetings, and reminders of upcoming deadlines, etc.</p>
                <p>You are not required to complete this form.  If we obtain your signature on this form by conditioning our service on your consent, your consent will not be valid.  Your consent is valid for the amount of time that you specify.  If you do not specify the duration of your consent, your consent is valid for a period of three years from the date you have signed the consent.</p>
                <p>The undersigned hereby consents to the use; by CER Financial Services / Easy Loan 2, its owner, employees, and staff; of any and all tax return information pertaining to:</p>

                <ul>
                    <li>Direct questions, inquiries or requests you make regarding your tax returns, situation, or issues.</li>
                    <li>Requests you make for copies of your documents, figures, numbers, or information.</li>
                    <li>Upon your direct request in order to connect you with another professional in another field or profession so you may gain further information on a certain topic not available or in the realm of expertise of this firm.</li>
                    <li>Notification of important tax law changes.</li>
                    <li>Notification of changes or information that will impact our engagement, such as a change of firm website address, email address, physical location, or other such contact, biographical or geographical information.</li>
                    <li>Other reasonable business purposes including but not limited to: appointment reminders, holiday mailings, birthday greetings and other informational mailings.</li>
                </ul>

                <p>The tax information may not be disclosed or used by CER Financial Services / Easy Loan 2, its owner, employees, or staff, for any purpose other than that which is permitted by this consent document.</p>
                <p>If you believe your tax return information has been disclosed or used improperly in a manner unauthorized by law or without your permission, you may contact the Treasury Inspector General for Tax Administration (TIGTA) by telephone at 1-800-366-4484 by email at complaints@tigta.treas.gov.</p>
                <p>Also be aware if CER Financial Services / Easy Loan 2 has provided you service and your Tax Refund was interceded by the IRS, you are still responsible for fee’s for services rendered. If payment is not received within 30 days from the time IRS interceded your refund. Your account will be sent to a collection agency and you will be responsible for additional fee’s.</p>
                <p>Sincerely</p>
                <p>CER Financial Services</p>
                <p>Easy Loan 2</p>
            </div>

            <div class="col-sm-12">
                <h4 class="txt-color text-center">SIGN IN BOX BELOW WITH YOUR MOUSE OR A STYLUS IF USING A
                TOUCHSCREEN ENABLED DEVICE</h4>
                <div class="row">
                    <div class="col-sm-8">
                    <span class="text-clr">Signature*</span>
                        <div class="wrapper-sign">
                        <?php
                            if(isset($tax109[2]) && !empty($tax109[2])){
                            ?>
                            <img src="<?=$site_url."/upload/".$tax0."/".$tax109[2];?>" class="view-sign"/>
                            <?php 
                            } 
                           else{
                           ?> 
                            <canvas id="signature-pad-2" class="signature-pad" width=400 height=200></canvas>
                           <?php } ?>
                      </div>
                      <div class="sign-btn-bx">
                          <button id="clear-3" class="clear">Clear</button>
                      </div>


                        <?php /* ?>
                        <span class="text-clr">Signature*</span>
                        <canvas id="myCanvas"></canvas><br><br>
                        <input type="button" value="Reset" id='resetSign'>
                        <?php */ ?>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-clr mb-0">Date*</p>
                        <input type="text" id="datepicker" width="276" class="corner-zero" name="date[]" value="<?=$tax110[2];?>" <?=$D;?>>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <p class="text-clr mb-0">Print Taxpayer's Name*</p>
                <input type="text" class="form-control corner-zero" name="print_taxpayer_name[]" value="<?=$tax111[2];?>" <?=$D;?>>
            </div>
                <div class="col-sm-12">
                <?php 
                if(!isset($view) || empty($view)){ ?>
                    <button type="submit" value="Upload" name="Upload" class="btn btn-warning-set">Submit</button>
                    <!-- <button type="submit" class="btn btn-outline-danger float-right">Save</button> -->
                <?php } ?>  
                </div>             
        </div>
    </div>
</div>
</form>
</div><!-- container-fluid -->
</section>
<!-- Scroll to top -->
<a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
</a>
<!-- Scroll to top -->
</div>
<script>
    $(document).ready(function () {
        $('#yes-show').click(function () {
            $('.Spouse-show').show('700');
        });
        $('#no-show').click(function () {
            $('.Spouse-show').hide('700');
        });

//--dependents----
        $('#yes-show-1').click(function () {
            $('#dependents').show('700');
            
        });
        $('#no-show-1').click(function () {
            $('#dependents').hide('700');
            $('#dependent-1').hide('700');
            $('#dependent-2').hide('700');
            $('#dependent-3').hide('700');
            $('#dependent-4').hide('700');
        });


        $('#one').click(function () {
            $('#dependent-1').show('700');
            $('#dependent-2').hide('700');
            $('#dependent-3').hide('700');
            $('#dependent-4').hide('700');
        });
        $('#two').click(function () {
            $('#dependent-1').show('700');
            $('#dependent-2').show('700');
            $('#dependent-3').hide('700');
            $('#dependent-4').hide('700');
        });
        $('#three').click(function () {
            $('#dependent-1').show('700');
            $('#dependent-2').show('700');
            $('#dependent-3').show('700');
            $('#dependent-4').hide('700');
        });
        $('#four').click(function () {
            $('#dependent-1').show('700');
            $('#dependent-2').show('700');
            $('#dependent-3').show('700');
            $('#dependent-4').show('700');
        });


        $('#yes-show-2').click(function () {
            $('.daycare').show('700');
        });
        $('#no-show-2').click(function () {
            $('.daycare').hide('700');
        });
        $('.Checking').click(function () {
            $('#bank').show('700');
        });
        $('.Checking-1').click(function () {
            $('#bank').hide('700');
        });
        $('#employer').click(function () {
            $('#Insurance').show('700');
        });
        $('.Medicare').click(function () {
            $('#Insurance').hide('700');
        });

        //Education Information
        $('#yesCollege').click(function () {
            $('#household').show('700');
        });
        $('#noCollege').click(function () {
            $('#household').hide('700');
        });

        $('#people1').click(function () {
            $('#attending1').show('700');
            $('#attending2').hide('700');
            $('#attending3').hide('700');
        });
        $('#people2').click(function () {
            $('#attending1').show('700');
            $('#attending2').show('700');
            $('#attending3').hide('700');
        });
        $('#people3').click(function () {
            $('#attending1').show('700');
            $('#attending2').show('700');
            $('#attending3').show('700');
        });

        //Employment Information
        $('#W2').click(function () {
            $('#show-file').toggle('700');
        });
        $('#Employment').click(function () {
            $('#show-business').toggle('700');
        });

        // Deductions
        $('#click-show1').click(function () {
            $('#documents-show').show('700');
        });
        $('#click-show2').click(function () {
            $('#documents-show').show('700');
        });
        $('#click-show3').click(function () {
            $('#documents-show').show('700');
        });
        $('#click-show4').click(function () {
            $('#documents-show').show('700');
        });
        $('#click-show5').click(function () {
            $('#documents-show').show('700');
        });
        $('#click-show6').click(function () {
            $('#documents-show').show('700');
        });
        $('#click-show7').click(function () {
            $('#documents-show').show('700');
        });
        $('#click-show8').click(function () {
            $('#documents-show').show('700');
        });
        $('#click-show9').click(function () {
            $('#documents-show').show('700');
        });
        $('#click-show10').click(function () {
            $('#documents-show').hide('700');
        });

        //Casualty & Loss
        $('#casualty-show').click(function () {
            $('#casualty').toggle('700');
        });
        $('#casualty-no').click(function () {
            $('#casualty').toggle('700');
        });


    });

//date picker////
$('#datepicker').datepicker({
    uiLibrary: 'bootstrap4'
});
$('#datepicker-one').datepicker({
    uiLibrary: 'bootstrap4'
});
$('#datepicker-two').datepicker({
    uiLibrary: 'bootstrap4'
});

</script>
<script>
    $(document).ready(function(){
        // $('#myCanvas').sign({
        //     resetButton: $('#resetSign'),
        //     lineWidth: 5,
        //     height:300,
        //     width:400
    });        
</script>

<style>
    .wrapper-sign{
      position: relative;
      height: 200px;
      -moz-user-select: none;
      -webkit-user-select: none;
      -ms-user-select: none;
      user-select: none;
      margin-bottom: 15px;
  }

  .signature-pad {
    position: absolute;
    left: 0;
    right: 0;
    top:0;
    bottom: 0;
    width: 100%;
    height: 100%;
    background-color: #fff;
}
.sign-btn-bx{
    margin-bottom: 30px;
}
</style>

<!-- signature pad -->
<script src="/signature_pad.umd.js"></script>
<script>
    var canvas = document.getElementById('signature-pad');
    var signaturePad = new SignaturePad(canvas, {
        // Necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
        backgroundColor: 'rgb(255, 255, 255)'
    });
    var canvas2 = document.getElementById('signature-pad-1');
    var signaturePad2 = new SignaturePad(canvas2, {
        // Necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
        backgroundColor: 'rgb(255, 255, 255)'
    });
    var canvas3 = document.getElementById('signature-pad-2');
    var signaturePad3 = new SignaturePad(canvas3, {
        // Necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
        backgroundColor: 'rgb(255, 255, 255)'
    });
// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
    canvas2.width = canvas2.offsetWidth * ratio;
    canvas2.height = canvas2.offsetHeight * ratio;
    canvas2.getContext("2d").scale(ratio, ratio);
    canvas3.width = canvas3.offsetWidth * ratio;
    canvas3.height = canvas3.offsetHeight * ratio;
    canvas3.getContext("2d").scale(ratio, ratio);
}
window.onresize = resizeCanvas;
resizeCanvas();
function download(dataURL, filename, basesignid) {
  var blob = dataURLToBlob(dataURL,basesignid);
  var url = window.URL.createObjectURL(blob,basesignid);
  
  var a = document.createElement("a");
  a.style = "display: none";
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
//   window.URL.revokeObjectURL(url);
}
// One could simply use Canvas#toBlob method instead, but it's just to show
// that it can be done using result of SignaturePad#toDataURL.
function dataURLToBlob(dataURL,basesignid) {
  // Code taken from https://github.com/ebidel/filer.js
  
  var parts = dataURL.split(';base64,');
  var contentType = parts[0].split(":")[1];
  $(basesignid).val(parts[1])
  var raw = window.atob(parts[1]);
  var rawLength = raw.length;
  var uInt8Array = new Uint8Array(rawLength);
  for (var i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }
//   return new Blob([uInt8Array], { type: contentType });
}
$(document).on('mouseup','.signature-pad',function(e){
    e.preventDefault();
    curr_pad_id = $(this).attr('id');
    if(curr_pad_id == 'signature-pad'){
        if (signaturePad.isEmpty()) {
            return alert("Please provide a signature first.");
        }
        basesign = '<input type="hidden" name="basesign[]" id="base-sign">';
        basesignid = '#base-sign';
        if($("#".basesignid).val()!=undefined){
            $(this).remove();
        }
        $( "#"+curr_pad_id ).after( basesign );
        var data = signaturePad.toDataURL('image/png');
        $( "#"+curr_pad_id ).after( basesign );
        download(data, "signature.png", basesignid); 
    }
    else if(curr_pad_id == 'signature-pad-1'){
        if (signaturePad2.isEmpty()) {
            return alert("Please provide a signature first.");
        }
        basesign = '<input type="hidden" name="basesign[]" id="base-sign-1">';
        basesignid = '#base-sign-1';
        if($("#".basesignid).val()!=undefined){
            $(this).remove();
        }
        var data = signaturePad2.toDataURL('image/png');
        $( "#"+curr_pad_id ).after( basesign );
        download(data, "signature.png", basesignid); 
    }
    else if(curr_pad_id == 'signature-pad-2'){
        if (signaturePad3.isEmpty()) {
            return alert("Please provide a signature first.");
        }
        basesign = '<input type="hidden" name="basesign[]" id="base-sign-2">';
        basesignid = '#base-sign-2';
        if($("#".basesignid).val()!=undefined || $("#".basesignid).val()!=null){
            $(this).remove();
        }
        var data = signaturePad3.toDataURL('image/png');
        $( "#"+curr_pad_id ).after( basesign );
        download(data, "signature.png", basesignid); 
    }
     
});
// document.getElementById('save-png').addEventListener('click', function () {
//   if (signaturePad.isEmpty()) {
//     return alert("Please provide a signature first.");
//   }
//     var data = signaturePad.toDataURL('image/png');
//     download(data, "signature.png");
// });
// document.getElementById('save-jpeg').addEventListener('click', function () {
//   if (signaturePad.isEmpty()) {
//     return alert("Please provide a signature first.");
// }
// var data = signaturePad.toDataURL('image/jpeg');
// download(data, "signature.jpg");
// });
// document.getElementById('save-svg').addEventListener('click', function () {
//   if (signaturePad.isEmpty()) {
//     return alert("Please provide a signature first.");
// }
// var data = signaturePad.toDataURL('image/svg+xml');
// console.log(atob(data.split(',')[1]));
// download(data, "signature.svg");
// });
$(document).on("click",".clear",function(e){
    e.preventDefault();
    curr_id = $(this).attr('id');
    if(curr_id == 'clear-1'){
        signaturePad.clear();
    }
    else if(curr_id == 'clear-2'){
        signaturePad2.clear();
    }
    else if(curr_id == 'clear-3'){
        signaturePad3.clear();
    }
});
function validateForm(event){
    if ($("#base-sign").val() === undefined || $("#base-sign").val() === null || $("#base-sign-1").val() === undefined || $("#base-sign-1").val() === null || $("#base-sign-2").val() === undefined || $("#base-sign-2").val() === null) {
        event.preventDefault();
     alert('Signature can\'t be empty');
     return false;
    }
    else{
        return true;
    }  
}
</script>

<?php include('../../footer.php'); ?>