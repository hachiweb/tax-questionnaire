<?php
error_reporting(0);
$auth ="public";
include('../../header.php');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Register to get affliliate link</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
              <li class="breadcrumb-item active">Register to get affliliate link</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-2"></div>
          <!-- right column -->
          <div class="col-md-8">
            <!-- general form elements disabled -->
            <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Register</h3>
            </div> <!-- card-warning -->
              <!-- /.card-header -->
              <div class="card-body">
                <form action="sub-ref-reg.php" role="form" method="post">
                  <div class="row">
                  <div class="col-md-1"></div> 
                  <div class="col-md-10">
                  <!-- input states -->
                  <div class="form-group">
                    <label class="control-label" for="name"><i class="far fa-user"></i> Name</label>
                    <input type="text" class="form-control is-warning" name="Name" id="name" placeholder="Enter ...">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="Company"><i class="far fa-building"></i> Company Name</label>
                    <input type="text" class="form-control is-warning" name="Company" id="Company" placeholder="Enter ...">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="Email"><i class="fas fa-envelope-open-text"></i> Email</label>
                    <input type="email" class="form-control is-warning" name="Email" id="Email" placeholder="Enter ...">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="Contact"><i class="far fa-id-badge"></i> Contact No.</label>
                    <input type="text" class="form-control is-warning" name="Contact" id="Contact" placeholder="Enter ...">
                  </div>
                  <div>
                    <input type="hidden" name="referral_code" value="<?=rand(1000000, 10000000);?>">
                  </div>
                  <div class="form-group text-center">
                    <button type="submit" class="btn btn-warning">Submit</button>
                  </div>
                  </div> 
                  </div>
                </form>
            </div><!-- /.card-body -->
          </div> <!--/.col (right) -->
        </div> <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
include('../../footer.php');
?>