<?php
error_reporting(0);
require_once('../../dbconnect.php');
$db = new DB();
$referral_code = $_POST['referral_code'];
$referred_by = $_POST['referred_by'];
$last_id = "SELECT id FROM `tax_preparation_questionnaire` ORDER BY id DESC";
$last_id = $db->executeQuery($last_id);
$last_id = mysqli_fetch_assoc($last_id);
$current_id = (int)$last_id['id']+1;
$target_dir = "../../upload/".$current_id;
if(!is_dir($target_dir)){
    if(mkdir($target_dir,0777, true)){
        // print_r('here');
    }
    else{
        // print_r('there');
    }
}
// exit();
if ($current_id) {
    $count = 0;
    $signature_csv = '';
    foreach($_POST['basesign'] as $key => $value){
        $pref = '/sign_'.$current_id.str_replace(' ','',date("Y-m-d H:i:s")).$count;
        $data = base64_decode($value);
        $signaturedata = $target_dir.$pref.'.png';
        $signature = $pref.'.png';
        file_put_contents($signaturedata, $data);
        if($signature_csv == ''){
            $signature_csv = $signature;
        }
        else{
            $signature_csv = $signature_csv.','.$signature;
        }
        $count++;
    }
}
if (!empty($_FILES["driver_license_ss_card"]["name"])) {
    foreach ($_FILES["driver_license_ss_card"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["driver_license_ss_card"]["tmp_name"][$key]);
            $nameDLSC = str_replace(' ','',$_FILES["driver_license_ss_card"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameDLSC");
            if ((!empty($driver_license_ss_card))) {
                $driver_license_ss_card = $driver_license_ss_card.",";
            }
            $driver_license_ss_card .= $nameDLSC;
        }
    }
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`driver_license_ss_card`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$driver_license_ss_card'";
}
$referral_site_company = $_POST['referral_site_company'];
$are_you_a_new_client = $_POST['are_you_a_new_client'];
$filling_status = $_POST['filling_status'];
$taxpayer_fname = $_POST['taxpayer_fname'];
$taxpayer_lname = $_POST['taxpayer_lname'];
$taxpayer_address = $_POST['taxpayer_address'];
$taxpayer_city = $_POST['taxpayer_city'];
$taxpayer_state = $_POST['taxpayer_state'];
$taxpayer_zip = $_POST['taxpayer_zip'];
$tax_payer_date_of_birth = $_POST['tax_payer_date_of_birth'];
$social_security_number = $_POST['social_security_number'];
$taxpayer_occupation = $_POST['taxpayer_occupation'];
$taxpayer_email = $_POST['taxpayer_email'];
$taxpayer_phone = $_POST['taxpayer_phone'];
$tax_payer_identification = $_POST['tax_payer_identification'];
$irs_ip_pin = $_POST['irs_ip_pin'];
$are_you_married = $_POST['are_you_married'];
$do_you_have_any_children = $_POST['do_you_have_any_children'];
$dependent_care_aftercare_expenses = $_POST['dependent_care_aftercare_expenses'];
$advance_up_to_6000 = $_POST['advance_up_to_6000'];
$refund_advance_deposited = $_POST['refund_advance_deposited'];
$health_insurance_coverage = $_POST['health_insurance_coverage'];
$dependent_have_health_insurance_coverage = $_POST['dependent_have_health_insurance_coverage'];
$healthcare_coverage_as = $_POST['healthcare_coverage_as'];
if (!empty($_FILES["health_care_forms"]["name"])) {
    foreach ($_FILES["health_care_forms"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["health_care_forms"]["tmp_name"][$key]);
            $nameHCF = str_replace(' ','',$_FILES["health_care_forms"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameHCF");
            if ((!empty($health_care_forms))) {
                $health_care_forms = $health_care_forms.",";
            }
            $health_care_forms .= $nameHCF;
        }
    }
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`health_care_forms`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$health_care_forms'";
}
$attend_college = $_POST['attend_college'];
$type_of_income = (implode(",",$_POST['type_of_income']));
$employment_additional_information = $_POST['employment_additional_information'];
$deductions_that_apply = (implode(",",$_POST['deductions_that_apply']));
$deductions_doc = $_POST['deductions_doc'];
if (!empty($_FILES["deductions_doc"]["name"])) {
    foreach ($_FILES["deductions_doc"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["deductions_doc"]["tmp_name"][$key]);
            $nameADC = str_replace(' ','',$_FILES["deductions_doc"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameADC");
            if ((!empty($deductions_doc))) {
                $deductions_doc = $deductions_doc.",";
            }
            $deductions_doc .= $nameADC;
        }
    }
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`deductions_doc`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$deductions_doc'";
}
$deductions_additional_information = $_POST['deductions_additional_information'];
$casualty_area = $_POST['casualty_area'];
$living_benefits_and_business_iul = $_POST['living_benefits_and_business_iul'];
$interested_in_credit_restoration = $_POST['interested_in_credit_restoration'];
$interested_in_retirement_planning = $_POST['interested_in_retirement_planning'];
$due_diligence_compliance = (implode(",",$_POST['due_diligence_compliance']));
$qualifying_dependent = $_POST['qualifying_dependent'];
$date = (implode(",",$_POST['date']));
$print_taxpayer_name = (implode(",",$_POST['print_taxpayer_name']));
if ($are_you_married=="Yes") {
    $spouse_fname = $_POST['spouse_fname'];
    $spouse_lname = $_POST['spouse_lname'];
    $spouse_date_of_birth = $_POST['spouse_date_of_birth'];
    $spouse_social_security_number = $_POST['spouse_social_security_number'];
    $spouse_occupation = $_POST['spouse_occupation'];
    $spouse_identification = $_POST['spouse_identification'];
    foreach ($_FILES["spouse_driver_s_license_ss_card"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["spouse_driver_s_license_ss_card"]["tmp_name"][$key]);
            $nameSDLSC = str_replace(' ','',$_FILES["spouse_driver_s_license_ss_card"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameSDLSC");
            if ((!empty($spouse_driver_s_license_ss_card))) {
                $spouse_driver_s_license_ss_card = $spouse_driver_s_license_ss_card.",";
            }
            $spouse_driver_s_license_ss_card .= $nameSDLSC;
        }
    }
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`spouse_fname`, `spouse_lname`, `spouse_date_of_birth`, `spouse_social_security_number`, `spouse_occupation`, `spouse_identification`, `spouse_driver_s_license_ss_card`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$spouse_fname', '$spouse_lname', '$spouse_date_of_birth', '$spouse_social_security_number', '$spouse_occupation', '$spouse_identification', '$spouse_driver_s_license_ss_card'";
}
if ($do_you_have_any_children=="Yes") {
    $how_many_children = $_POST['how_many_children'];
    $dependent_1_fname = $_POST['dependent_1_fname'];
    $dependent_1_lname = $_POST['dependent_1_lname'];
    $dependent_1_date_of_birth = $_POST['dependent_1_date_of_birth'];
    $dependent_1_social_security_number = $_POST['dependent_1_social_security_number'];
    $dependent_1_relationship_to_you = $_POST['dependent_1_relationship_to_you'];
    foreach ($_FILES["dependent_1_social_security_card"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["dependent_1_social_security_card"]["tmp_name"][$key]);
            $nameD1SC = str_replace(' ','',$_FILES["dependent_1_social_security_card"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameD1SC");
            if ((!empty($dependent_1_social_security_card))) {
                $dependent_1_social_security_card = $dependent_1_social_security_card.",";
            }
            $dependent_1_social_security_card .= $nameD1SC;
        }
    }
    $dependent_2_fname = $_POST['dependent_2_fname'];
    $dependent_2_lname = $_POST['dependent_2_lname'];
    $dependent_2_date_of_birth = $_POST['dependent_2_date_of_birth'];
    $dependent_2_social_security_number = $_POST['dependent_2_social_security_number'];
    $dependent_2_relationship_to_you = $_POST['dependent_2_relationship_to_you'];
    foreach ($_FILES["dependent_2_social_security_card"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["dependent_2_social_security_card"]["tmp_name"][$key]);
            $nameD2SC = str_replace(' ','',$_FILES["dependent_2_social_security_card"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameD2SC");
            if ((!empty($dependent_2_social_security_card))) {
                $dependent_2_social_security_card = $dependent_2_social_security_card.",";
            }
            $dependent_2_social_security_card .= $nameD2SC;
        }
    }
    $dependent_3_fname = $_POST['dependent_3_fname'];
    $dependent_3_lname = $_POST['dependent_3_lname'];
    $dependent_3_date_of_birth = $_POST['dependent_3_date_of_birth'];
    $dependent_3_social_security_number = $_POST['dependent_3_social_security_number'];
    $dependent_3_relationship_to_you = $_POST['dependent_3_relationship_to_you'];
    foreach ($_FILES["dependent_3_social_security_card"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["dependent_3_social_security_card"]["tmp_name"][$key]);
            $nameD3SC = str_replace(' ','',$_FILES["dependent_3_social_security_card"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameD3SC");
            if ((!empty($dependent_3_social_security_card))) {
                $dependent_3_social_security_card = $dependent_3_social_security_card.",";
            }
            $dependent_3_social_security_card .= $nameD3SC;
        }
    }
    $dependent_4_fname = $_POST['dependent_4_fname'];
    $dependent_4_lname = $_POST['dependent_4_lname'];
    $dependent_4_date_of_birth = $_POST['dependent_4_date_of_birth'];
    $dependent_4_social_security_number = $_POST['dependent_4_social_security_number'];
    $dependent_4_relationship_to_you = $_POST['dependent_4_relationship_to_you'];
    foreach ($_FILES["dependent_4_social_security_card"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["dependent_4_social_security_card"]["tmp_name"][$key]);
            $nameD4SC = str_replace(' ','',$_FILES["dependent_4_social_security_card"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameD4SC");
            if ((!empty($dependent_4_social_security_card))) {
                $dependent_4_social_security_card = $dependent_4_social_security_card.",";
            }
            $dependent_4_social_security_card .= $nameD4SC;
        }
    }
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`how_many_children`, `dependent_1_fname`, `dependent_1_lname`, `dependent_1_date_of_birth`, `dependent_1_social_security_number`, `dependent_1_relationship_to_you`, `dependent_1_social_security_card`, `dependent_2_fname`, `dependent_2_lname`, `dependent_2_date_of_birth`, `dependent_2_social_security_number`, `dependent_2_relationship_to_you`, `dependent_2_social_security_card`, `dependent_3_fname`, `dependent_3_lname`, `dependent_3_date_of_birth`, `dependent_3_social_security_number`, `dependent_3_relationship_to_you`, `dependent_3_social_security_card`, `dependent_4_fname`, `dependent_4_lname`, `dependent_4_date_of_birth`, `dependent_4_social_security_number`, `dependent_4_relationship_to_you`, `dependent_4_social_security_card`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$how_many_children', '$dependent_1_fname', '$dependent_1_lname', '$dependent_1_date_of_birth', '$dependent_1_social_security_number', '$dependent_1_relationship_to_you', '$dependent_1_social_security_card', '$dependent_2_fname', '$dependent_2_lname', '$dependent_2_date_of_birth', '$dependent_2_social_security_number', '$dependent_2_relationship_to_you', '$dependent_2_social_security_card', '$dependent_3_fname', '$dependent_3_lname', '$dependent_3_date_of_birth', '$dependent_3_social_security_number', '$dependent_3_relationship_to_you', '$dependent_3_social_security_card', '$dependent_4_fname', '$dependent_4_lname', '$dependent_4_date_of_birth', '$dependent_4_social_security_number', '$dependent_4_relationship_to_you', '$dependent_4_social_security_card'";
}
if ($dependent_care_aftercare_expenses=="Yes") {
    $daycare_aftercare_for_which_dependent = (implode(",",$_POST['daycare_aftercare_for_which_dependent']));
    $amount_paid = $_POST['amount_paid'];
    $provider_name = $_POST['provider_name'];
    $ein = $_POST['ein'];
    $provider_address = $_POST['provider_address'];
    $provider_city = $_POST['provider_city'];
    $provider_state = $_POST['provider_state'];
    $provider_zip = $_POST['provider_zip'];
    $daycare_aftercare_form = $_POST['daycare_aftercare_form'];
    foreach ($_FILES["daycare_aftercare_form"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["daycare_aftercare_form"]["tmp_name"][$key]);
            $nameDACF = str_replace(' ','',$_FILES["daycare_aftercare_form"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameDACF");
            if ((!empty($daycare_aftercare_form))) {
                $daycare_aftercare_form = $daycare_aftercare_form.",";
            }
            $daycare_aftercare_form .= $nameDACF;
        }
    }
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`dependent_care_aftercare_expenses`, `daycare_aftercare_for_which_dependent`, `amount_paid`, `provider_name`, `ein`, `provider_address`, `provider_city`, `provider_state`, `provider_zip`, `daycare_aftercare_form`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$dependent_care_aftercare_expenses', '$daycare_aftercare_for_which_dependent', '$amount_paid', '$provider_name', '$ein', '$provider_address', '$provider_city', '$provider_state', '$provider_zip', '$daycare_aftercare_form'";
}
if ($refund_advance_deposited=="Checking / Savings") {
    $bank_name = $_POST['bank_name'];
    $type_of_account = $_POST['type_of_account'];
    $bank_routing_number = $_POST['bank_routing_number'];
    $bank_account_number = $_POST['bank_account_number'];
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`bank_name`, `type_of_account`, `bank_routing_number`, `bank_account_number`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$bank_name', '$type_of_account', '$bank_routing_number', '$bank_account_number'";
}
if ($healthcare_coverage_as=="Employer") {
    $insurance_premium_paid = $_POST['insurance_premium_paid'];
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`insurance_premium_paid`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$insurance_premium_paid'";
}
if ($attend_college=="Yes") {
    $people_attending_college = $_POST['people_attending_college'];
    $person_1_name_attending_college = $_POST['person_1_name_attending_college'];
    $person_1_pay_for_college = $_POST['person_1_pay_for_college'];
    $person_1_college_name = $_POST['person_1_college_name'];
    foreach ($_FILES["person_1_college_form_receipts_etc"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["person_1_college_form_receipts_etc"]["tmp_name"][$key]);
            $nameP1CFR = str_replace(' ','',$_FILES["person_1_college_form_receipts_etc"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameP1CFR");
            if ((!empty($person_1_college_form_receipts_etc))) {
                $person_1_college_form_receipts_etc = $person_1_college_form_receipts_etc.",";
            }
            $person_1_college_form_receipts_etc .= $nameP1CFR;
        }
    }
    $person_2_name_attending_college = $_POST['person_2_name_attending_college'];
    $person_2_pay_for_college = $_POST['person_2_pay_for_college'];
    $person_2_college_name = $_POST['person_2_college_name'];
    foreach ($_FILES["person_2_college_form_receipts_etc"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["person_2_college_form_receipts_etc"]["tmp_name"][$key]);
            $nameP2CFR = str_replace(' ','',$_FILES["person_2_college_form_receipts_etc"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameP2CFR");
            if ((!empty($person_2_college_form_receipts_etc))) {
                $person_2_college_form_receipts_etc = $person_2_college_form_receipts_etc.",";
            }
            $person_2_college_form_receipts_etc .= $nameP2CFR;
        }
    }
    $person_3_name_attending_college = $_POST['person_3_name_attending_college'];
    $person_3_pay_for_college = $_POST['person_3_pay_for_college'];
    $person_3_college_name = $_POST['person_3_college_name'];
    foreach ($_FILES["person_3_college_form_receipts_etc"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["person_3_college_form_receipts_etc"]["tmp_name"][$key]);
            $nameP3CFR = str_replace(' ','',$_FILES["person_3_college_form_receipts_etc"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameP3CFR");
            if ((!empty($person_3_college_form_receipts_etc))) {
                $person_3_college_form_receipts_etc = $person_3_college_form_receipts_etc.",";
            }
            $person_3_college_form_receipts_etc .= $nameP3CFR;
        }
    }
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`people_attending_college`, `person_1_name_attending_college`, `person_1_pay_for_college`, `person_1_college_name`, `person_1_college_form_receipts_etc`, `person_2_name_attending_college`, `person_2_pay_for_college`, `person_2_college_name`, `person_2_college_form_receipts_etc`, `person_3_name_attending_college`, `person_3_pay_for_college`, `person_3_college_name`, `person_3_college_form_receipts_etc`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$people_attending_college', '$person_1_name_attending_college', '$person_1_pay_for_college', '$person_1_college_name', '$person_1_college_form_receipts_etc', '$person_2_name_attending_college', '$person_2_pay_for_college', '$person_2_college_name', '$person_2_college_form_receipts_etc', '$person_3_name_attending_college', '$person_3_pay_for_college', '$person_3_college_name', '$person_3_college_form_receipts_etc'";
}
if (in_array("Self Employment", $_POST['type_of_income'])) {
    $name_of_business = $_POST['name_of_business'];
    $description_of_business = $_POST['description_of_business'];
    $vehicle_description = $_POST['vehicle_description'];
    $date_in_service = $_POST['date_in_service'];
    $business_miles = $_POST['business_miles'];
    $total_miles_driven = $_POST['total_miles_driven'];
    foreach ($_FILES["self_employment_documents"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["self_employment_documents"]["tmp_name"][$key]);
            $nameSED = str_replace(' ','',$_FILES["self_employment_documents"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameSED");
            if ((!empty($self_employment_documents))) {
                $self_employment_documents = $self_employment_documents.",";
            }
            $self_employment_documents .= $nameSED;
        }
    }
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`name_of_business`, `description_of_business`, `vehicle_description`, `date_in_service`, `business_miles`, `total_miles_driven`, `self_employment_documents`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$name_of_business', '$description_of_business', '$vehicle_description', '$date_in_service', '$business_miles', '$total_miles_driven', '$self_employment_documents'";
}
if (in_array("W2", $_POST['type_of_income'])) {
    $income_documents = $_POST['income_documents'];
    foreach ($_FILES["income_documents"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = str_replace(' ','',$_FILES["income_documents"]["tmp_name"][$key]);
            $nameIDS = str_replace(' ','',$_FILES["income_documents"]["name"][$key]);
            move_uploaded_file($tmp_name, "$target_dir/$nameIDS");
            if ((!empty($income_documents))) {
                $income_documents = $income_documents.",";
            }
            $income_documents .= $nameIDS;
        }
    }
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`income_documents`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$income_documents'";
}
if ($casualty_area=="Yes") {
    $casualty_city_and_state = $_POST['casualty_city_and_state'];
    if ((!empty($i))) {
        $i = $i.",";
    }
    $i .= "`casualty_city_and_state`";
    if ((!empty($v))) {
        $v = $v.",";
    }
    $v .= "'$casualty_city_and_state'";
}
if ((!empty($i))) {
    $i = $i.",";
}else{
    $i = "";
}
if ((!empty($v))) {
    $v = $v.",";
}else{
    $v = "";
}
$sql = "INSERT INTO `tax_preparation_questionnaire` ($i`referral_code`, `referred_by`, `referral_site_company`, `are_you_a_new_client`, `filling_status`, `taxpayer_fname`, `taxpayer_lname`, `taxpayer_address`, `taxpayer_city`, `taxpayer_state`, `taxpayer_zip`, `tax_payer_date_of_birth`, `social_security_number`, `taxpayer_occupation`, `taxpayer_email`, `taxpayer_phone`, `tax_payer_identification`, `irs_ip_pin`, `are_you_married`, `do_you_have_any_children`, `advance_up_to_6000`, `refund_advance_deposited`, `health_insurance_coverage`, `dependent_have_health_insurance_coverage`, `healthcare_coverage_as`, `attend_college`, `type_of_income`, `employment_additional_information`, `deductions_that_apply`, `deductions_additional_information`, `casualty_area`, `living_benefits_and_business_iul`, `interested_in_credit_restoration`, `interested_in_retirement_planning`, `due_diligence_compliance`, `qualifying_dependent`, `signature`, `date`, `print_taxpayer_name`) VALUES ($v'$referral_code', '$referred_by', '$referral_site_company', '$are_you_a_new_client', '$filling_status', '$taxpayer_fname', '$taxpayer_lname', '$taxpayer_address', '$taxpayer_city', '$taxpayer_state', '$taxpayer_zip', '$tax_payer_date_of_birth', '$social_security_number', '$taxpayer_occupation', '$taxpayer_email', '$taxpayer_phone', '$tax_payer_identification', '$irs_ip_pin', '$are_you_married', '$do_you_have_any_children', '$advance_up_to_6000', '$refund_advance_deposited', '$health_insurance_coverage', '$dependent_have_health_insurance_coverage', '$healthcare_coverage_as', '$attend_college', '$type_of_income', '$employment_additional_information', '$deductions_that_apply', '$deductions_additional_information', '$casualty_area',  '$living_benefits_and_business_iul', '$interested_in_credit_restoration', '$interested_in_retirement_planning', '$due_diligence_compliance', '$qualifying_dependent', '$signature_csv', '$date', '$print_taxpayer_name')";
$result = $db->executeQuery($sql);
if ($result) {
    header("location: thank-you.php");
} else {
    $success = "Your form could not be submitted";
}
echo $success;
?>