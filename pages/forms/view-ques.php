<?php
$auth ="admin";
include '../../header.php';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Questionnaire Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
              <li class="breadcrumb-item active">All Questionnaire Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <?php
                $db = new DB();
                $sql = "SELECT * FROM `tax_preparation_questionnaire`";
                $result = $db->executeQuery($sql);
                ?>
              <div class="card-header">
                <h3 class="card-title">Questionnaire</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-striped">
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Tax Payer Name</th>
                    <th>Tax Payer Email</th>
                    <th>Tax Payer Phone</th>
                    <th>Referred By</th>
                    <th>Referral Code</th>
                    <th>Referral Site / Company</th>
                    <th>Link</th>
                  </tr>
                  <?php
                  while ($tax = mysqli_fetch_array($result)) {?>
                  <tr>
                    <td><?=$tax['id'];?>.</td>
                    <td><?=$tax['taxpayer_fname']." ".$tax['taxpayer_lname'];?></td>
                    <td><?=$tax['taxpayer_email'];?></td>
                    <td><?=$tax['taxpayer_phone'];?></td>
                    <td><?=(!empty($tax['referred_by']))?$tax['referred_by']:"N/A";?></td>
                    <td><?=(!empty($tax['referral_code']))?$tax['referral_code']:"N/A";?></td>
                    <td><?=(!empty($tax['referral_site_company']))?$tax['referral_site_company']:"N/A";?></td>
                    <td><a href="<?=$site_url?>/pages/forms/ques-form.php?view=<?=$tax['id'];?>">View</a></td>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
include '../../footer.php';
?>