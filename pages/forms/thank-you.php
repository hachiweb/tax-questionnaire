<?php
error_reporting(0);
$auth ="public";
include('../../header.php'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Success</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.php">Home</a></li>
              <li class="breadcrumb-item active">Success</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">THANK YOU! Your submission has been successfully received!</h3>
              </div>
              <div class="card-body">
                <?php 
                $ref = $_GET['ref'];
                if (!empty($ref)) { ?>
                  <div class="form-group row">
                    <label for="ReferralCode" class="col-sm-2 control-label">Referral Code</label>
                    <div class="col-sm-10">
                      <p><?=$ref;?></p>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="ReferralLink" class="col-sm-2 control-label">Referral Link</label>
                    <div class="col-sm-10">
                      <a href="<?=$site_url?>/pages/forms/ques-form.php?ref=<?=$ref;?>"><?=$site_url?>/pages/forms/ques-form.php?ref=<?=$ref;?></a>
                    </div>
                  </div>
                  <div class="form-group row">
                    <?php
                    $l = $site_url."/pages/forms/ques-pub.php?ref=".$ref;
                    $s = "style='width:100%; height:100%;'";
                    $i = "<iframe src='";
                    $k = "'";
                    $j= "></iframe>";
                    $iframe = $i.$l.$k.$s.$j;
                    ?>
                    
                    <label for="iframe" class="col-sm-2 control-label">Embed iFrame Code</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="iframe" value="<?=$iframe?>" disabled>
                    </div>
                  </div>
                  <?php
                }
                ?>
                <h1 class="m-auto text-center">Thank You!</h1>
                </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-2"></div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>